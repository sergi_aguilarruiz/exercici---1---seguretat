# Firewall bastió
### Configuració inicial
**Ex.1**
![](assets/pf-1.png)

**Ex.2**
![](assets/pf-2.png)
![](assets/pf-3.png)
### Assegurar l'entorn

**Ex4**
Fem un nslookup de la primera pagina.
![](assets/pf-4.png)

Anem al menu-Interfaces-Lan i escollim l'opció "add"

![](assets/pf-6.png)

Ara configurem la "rule" com aquesta captura.

![](assets/pf-7.png)

Una vegada fet això reiniciem la cache del Firefox i comprovem que no podem entrar a la web.

![](assets/pf-8.png)

Ara fem el mateix amb la web muylinux.

![](assets/pf-10.png)

![](assets/pf-11.png)

**Ex5**

FTP

![](assets/pf-12.png)


![](assets/pf-13.png)

![](assets/pf-14.png)

HTTP

![](assets/pf-16.png)

![](assets/pf-17.png)


![](assets/pf-15.png)


**Ex6**
![](assets/pf-18.png)

![](assets/pf-19.png)

![](assets/pf-20.png)

**Ex7**

La diferencia es que amb l'acció Block no et deixa connectar-te directament i el reject tol el rato s'intenta connectar.

### DMZ

Configuració Xarxa DMZ

1- Configuració adreça IP DMZ en el firewall.

![](assets/pf-21.png)

2- Configuració servidor DMZ en el netplan.

![](assets/pf-23.png)

**Ex10**

![](assets/pf-31.png)

**Ex11**

Configuració Regla.

![](assets/pf-25.png)

Comprovació

![](assets/pf-26.png)

**Ex12**

Configuració Regla des de la LAN.

![](assets/pf-27.png)

Comprovació

En aquest cas he configurat una pagina en apache2, per la comprovació.

![](assets/pf-28.png)

Configuració Regla des de la WAN.

![](assets/pf-29.png)

Comprovació

En aquest cas he fet un curl des de la interfície WAN del firewall.

![](assets/pf-30.png)

### Monitorització

**Ex14**

En aquest cas des de el fpsense ho podem veure en la "ARP Table", que es troba dins de Diagnostics.

![](assets/pf-32.png)

**Ex15**

Comprovació de que no podem accedir, en aquest cas he utilitzat el wireshark.

![](assets/pf-33.png)

La informació la podem trobar o be en el whireshark capturant la interfície o en el pfsense ens anem a "Status" - "System Logs" i després escollim l'opció "Firewall".

**Exemple**

![](assets/pf-34.png)
