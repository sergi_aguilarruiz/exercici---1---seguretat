## NTOP NG

### Pràctica

**Instal·leu ntop al pfsense (gestor de paquets, afegir) per poder monitoritzar el tràfic de la xarxa des de la xarxa local a la WAN (i al contrari).**

**Recordeu activar NTOP anant a Diagnostics-NTOPNG settings.
Indiqueu a ntop quina interfície ha d'escoltar per recollir la informació.
L'usuari administrador de ntop és "admin". Poseu-hi contrasenya.**

En aquest cas he posat la contrasenya "P@ssw0rd" i aquesta es la configuració.

![](assets/ntop-1.png)

![](assets/ntop-2.png)

**Per entrar a la consola web de ntop: http://hostname:3000/ o http://localhost:3000/ o des del configurador de ntop.**

En el meu cas la ip es "10.10.0.1" i el port "3000"

![](assets/ntop3.png)

**Comproveu com canvien les estadístiques registrades per ntop**

En el cas del ntop cada X segons van canviant, de moment això es el que ha pogut registrar en la meva xarxa.

**Lan**

Seleccionem la interfície "em1" i després aquesta Opció

![](assets/ntop-7.png)

I et sortiren les estadístiques.

![](assets/ntop-9.png)

**Wan**

Seleccionem la interficie "em0" i despres aquesta Opció

![](assets/ntop-7.png)

I et sortiren les estadístiques.

![](assets/ntop-8.png)

**Mireu quins protocols són els que ocupen més tràfic a la xarxa (summary-traffic-protocol distribution).**

Wan

![](assets/ntop-10.png)

LAN

![](assets/NTOP-11.png)

**Mireu quins hosts locals ha detectat ntop (es pot forçar una detecció de hosts. Mira com es fa).**

Per fer una detecció de hosts anem a aquesta opció

![](assets/ntop-11.png)

Després seleccionem "Network Discovery" i despres li donem a aquesta opció, esperem uns segons i ens sortiran els hosts locals. En aquest cas nomes ha trobat 2.

![](assets/ntop-12.png)

![](assets/ntop-13.png)

Però si anem a "hosts" i han mes perque son els que ha detectat abans.

![](assets/ntop-14.png)

**Mireu quins són els hosts que generen més tràfic a la xarxa i a quines hores.**

Per saber això anem a la opció "Hosts" i després a "Top hosts"

![](assets/ntop-15.png)

Top Hosts en Lan

![](assets/ntop-16.png)

Top Hosts en WAN

![](assets/ntop-17.png)


**Mireu quins són els hosts remots als que ens hem connectat i quins protocols s'han fet servir, així com el tràfic que han generat.**

Per fer això hem d'anar a la opció "Flows"

![](assets/nto-18.png)

![](assets/ntop-19.png)

**Indiqueu quines altres informacions podeu veure amb ntop.**

Podem veure la localització dels hosts, la xarxa, els sistemes operatius dels hosts, els ports que s'han utilitzat.

![](assets/ntop-20.png)

El volum de descarrega

![](assets/ntop-21.png)

La informació del protocol ICMP i també ARP.

![](assets/ntop-22.png)

 També ens dona alertes.

![](assets/ntop-23.png)
