## Instal·lació de Nagios

**1- Actualitzar la maquina**

Primer hem de fer un "sudo apt-get update" i després un "sudo apt-get upgrade"

**2- Instal·lar el Nagios**

En el meu cas he instal·lat un ubuntu server de nou i no he afegit cap repositori, per tant només he fet la següent comanda.

~~~
saguilar@server:~$ curl https://assets.nagios.com/downloads/nagiosxi/install.sh | sudo sh
~~~

Quan s'acabi la instal·lació ens apareixera aquest missatge per accedir des de el navegador web.

~~~
CCM data imported OK.
RESULT=0
Running './F-startdaemons'...
Daemons started OK
RESULT=0
Running './Z-webroot'...
RESULT=0

Nagios XI Installation Complete!
--------------------------------

You can access the Nagios XI web interface by visiting:
    http://10.0.2.15/nagiosxi/

~~~

Primer de tot ens apareixerà aquesta finestra. Deixem tot predeterminada-ment, en el meu cas he escollit el "Modern Dark"

![](assets/nagi-1.png)

En aquesta finestra canviem la contrasenya de l'administrador en aquest cas es P@ssw0rd i canviem el e-mail, hem de posar el de l'Escola.

![](assets/nagi-2.png)

Llavors ens apareixerà una finestra de finalitzant la instal·lació

![](assets/nagi-3.png)

Aquesta finestra ens informa de que la instal·lació s'ha completat i ens informa de quines son les credencials per accedir al nagios.

![](assets/nagi-4.png)

Llavors per comprovar que s'ha instal·lat tot be iniciem sessió amb les credencials. Quan iniciem sessió ens tindria que aparèixer tot això

![](assets/nagi-5.png)

## Inventari d'actius.

En el meu cas he fet una xarxa nat, amb 1 servidor web, 1 client i un altre servidor amb el nagios.

**Ex2.**

Per fer això anem a la opció "home" i si ens fixem en la part esquerra hi ha una opció que posa "Run Auto-Discovery" doncs fem clic.

![](assets/nagi-6.png)

Ara escollim l'opció "New Auto-Discovery Job"
![](assets/nagi-7.png)

En la opció "Scan Target" posem la interficie de la xarxa i la "mask" en aquest cas es "10.0.2.0/24" una vegada fet això li donem a "Submit"

![](assets/nagi-8.png)

Es començara a escanejar.

![](assets/nagi-10.png)

En aquest cas hem posa que no ha trobat res perquè he fet un escaneig abans, si ens troba maquines noves li donem a la opció blava i ens sortira una finestra com aquesta. Llavors li hem de donar a "Next"

![](assets/nagi-11.png)

En aquesta finestra ens posa els resultats del escaneig.

![](assets/nagi-12.png)

Llavors aquí ens posa cada quant ens  monitoritzara els dispositius. Per ultim li donem a la opció "Finish"

![](assets/nagi-13.png)

Ara anem a la opció "host status" i ens apareixeran tots els hosts.

![](assets/nagi-14.png)


El host 2.5 es el meu client, el host 2.6 es el meu servidor web i el host 2.2 es la meva gateway, el localhost es el servidor que conte el nagios.

## Supervisio de serveis i dispositius.

**Ex 3**

Per fer això anem a "Run a Config Wizard" i despres a "Generic Network Device."

![](assets/nagi-16.png)

En aquesta finestra posem la IP del servidor web.

![](assets/nagi-17.png)

En la següent finestra li donem a "Next" i per ultim a "Finish"

![](assets/nagi-18.png)

Ara he apagat el servidor i he comprovat que el nagios enviava un correu.

![](assets/nagi-19.png)

**Ex 4**

He fet el següent.

![](assets/nagi-20.png)

![](assets/nagi-21.png)

![](assets/nagi-22.png)

![](assets/nagi-23.png)

![](assets/nagi-24.png)

**Ex 5**

Per fer això he anat al "Config Wizard" i he fet el següent.

 ![](assets/nagi-25.png)

 ![](assets/nagi-26.png)

Comprovació

 ![](assets/nagi-27.png)

 **Ex 6**

Per fer això he fet el següent.

![](assets/nagi-28.png)

![](assets/nagi-29.png)

![](assets/nagi-30.png)

![](assets/nagi-31.png)

![](assets/nagi-32.png)

![](assets/nagi-33.png)
