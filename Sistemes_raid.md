# SISTEMES RAID EN WINDOWS

## Gestió d'emmagatzemament en Windows

**Aneu a Inici->Herramientas Administrativas->Administración de equipos->Almacenamiento->Administración de discos.**

A sobre cada dispositiu d'emmagatzemament, amb el botó dret, veureu que disposem de vàries opcions. Explica que fan
"Abrir":
"Explorar":
"Marcar partición como activa": Aquesta opció activa la partició
"Cambiar letra y rutas de acceso de unidad": Aquesta opció canvia la lletra com es mostra el disc dur i la ruta per accedir a ell
"Formatear": Aquesta opció formata la partició o el disc
"Extender volumen": Aquesta opció agranda la partició
"Reducir volumen": Aquesta opció redueix la partició
"Agregar reflejo":
"Eliminar volumen": Aquesta opció elimina la partició
"Propiedades": Aquesta opció va a les propietats dels disc o partició
"Ayuda": Aquesta opció va a la ajuda de Windows

### Discos en RAID

**Primer crearem 3 discos durs SATA abans d'iniciar els equips virtuals, per poder treballar amb sistemes RAID, i després els iniciarem (crear particions, o en general volums i formatar):**

**Als paràmetres de la màquina virtual, aneu a Emmagatzematge->Controlador SATA->Afegir disc dur (25 GB)->Crear nou (disc dur de creixement dinàmic, amb opcions per defecte i nom "disc1", "disc2" i "disc3").**

![](assets/raid_1.png)

**Inicia el sistema, i inicialitza els discos en estil de particions MBR (GPT normalment per discos de més de 2 TB).****

Per fer aixó anem a "administrador de discos" i automaticament soritra una finestra per definir l'estil de particions

![](assets/raid_2.png)

**Comproveu que els discos NO es veuen al sistema (aneu a "Equipo" i mireu quins discos hi ha). L'espai no està assignat de cap manera, fins que es formatin (s'aplica un sistema d'arxius).**

![](assets/raid_3.png)

### Disc Distribuït: Anem a crear un disc distribuït agafant pedaços de diferents discos.

**Escull els discos a utilitzar i l'espai a cada disc (agafa el primer i segon discos): assigna en el segon disc només la meitat de l'espai.**

Per fer això anem a "administrador de discos" fem clic dret al disco 1 i seleccionem l'opció "nuevo volumen distribuido "

![](assets/raid_4.png)

**Comprova que el sistema avisa que es crearà un disc dinàmic. Què és un disc dinàmic?

Assigna-li la unitat E (serà el nom del volum). Abans de formatar, comprova quines opcions de format tens i què pots escollir. Què és una unitat d'assignació?

Comprova que el volum E agafa tot el primer disc i la meitat del segon. Quin tamany te?**

Un disc dinamic es un disc que te X GB pero que en realitat ocupa nomes els arxius que te dins

Per fer aixó una vegada escollit la primera opció s'obrira un assistent en la primera finestra escollim els dos discs, despres seleccionem el disco 2 i li canviem el tamany a 25GB que es la meitat

![](assets/raid_5.png)

Una vegada fet això soritra un missatge del disc dinamic i es començara a crear els discs distribuits

**Comprova que el volum E agafa tot el primer disc i la meitat del segon. Quin tamany te?**


![](assets/raid_20.png)

**Anar a inicio->Equipo per comprovar que tenim dos discos (volums amb unitats C i E).**


![](assets/raid_20.png)

**Ara amplia el volum E i agafa tot l'espai del disc2.**

Per fer això anem a "administrador de discos" fem clic dret al disco 1 i escollim l'opció "Extender volumen " fet això s'obrira el assistent i  afegim el disc 2

![](assets/raid_21.png)


![](assets/raid_22.png)

**Anar a inicio->Equipo per comprovar que tenim dos discos (C i E) però ara E ha crescut. Quin tamany te?**

99GB

**Reduir volum un altre cop a la meïtat del disc2 i afegir el disc3 sencer al volum. Com queda el volum E? Mireu com ho veu un usuari**

Per fer això anem a "administrador de discos" fem clic dret al disco 1 i escollim l'opció "Reducir volumen " fet això s'obrira el assistent i en tamany i escollim el tamany per afegir el disc 3 hem de primer anem a "administrador de discos" fem clic dret al disco 1 i escollim l'opció "Extender volumen " fet això s'obrira el assistent i  afegim el disc 3

![](assets/raid_23.png)

![](assets/raid_24.png)

**Crear un nou volum amb el que resta del disc2 i posar unitat F i comprovar que tenim tres discos o volums (C, E i F).**

Per fer això anem a "administrador de discos" en l'espai sense asingar del disc 3 fem clic dret - Nuevo volumen simple - i les opcions les deixem predeterminadament

![](assets/raid_25.png)

### RAID 0: Anem a crear un disc amb bandes sense paritat amb 2 i 3 discos

Eliminar volums E i F o torna enrera el snapshot. Tindrem els espais sense assignar.

Per fer això fem clic dret sobre els volums i l'opció de eliminar al final ens tindria que quedar així

![](assets/raid_26.png)

**Botó dret sobre disc1-> Crear volum seccionat. Seleccionar 2 discos i agafar 12 GB. Es pot escollir un espai diferent a cada disc? Per què?**

No, perquè la informació es reparteix entre els discs per iguals

**Anar a inicio->Equipo per comprovar que tenim dos discos (C i E) Quin tamany te E?**

E te 30GB

![](assets/raid_27.png)

**Amb la part no assignada del disc1 crear un nou volum seccionat, ara agafant espai dels 3 discos. Li assignem la unitat H.**

Disc 1

**Anar a administrador de discos - clic dret a disc1 en la part no asignada - opció nuevo volumen seccionado  i en la primera finestra de l'assistent agafem tots **

![](assets/raid_28.png)

**I en aquesta finestra ho deixem com aquesta captura en les darreres finestres les opcions seran predeterminadament **

![](assets/raid_29.png)

**Es pot agafar del disc 3 els 25 GB? Què passa si provem de fer el volum més gran agafant més espai del disc 3?**

Si, no ens deixara

**Anar a inicio->Equipo per comprovar que tenim tres discos (C, E, H). Quin tamany te H? Per què?**

114 GB perque es suma l'espai de tots els discos

![](assets/raid_30.png)

### RAID 1 (Mirall): Anem a crear un volum en mirall agafant 2 discos, a trencar-lo i simular fallades

**Eliminar volums E i H (exercici anterior) o restaurar snapshot. Tindrem els espais sense assignar.**

![](assets/raid_31.png)

**Botó dret sobre disc1-> Crear volum reflexat. Selecciona els discos 1 i 2 i agafa 12 GB a cadascun. Observa que no es pot escollir per a cada disc si no que agafa el mateix a tots dos.**

![](assets/raid_32.png)


![](assets/raid_33.png)

**Anar a inicio->Equipo per comprovar que tenim dos discos (C i E). Quin tamany te E? Creus que és coherent?**

De 11,7 GB si perque aquest sistema el que fa es que duplica els discs per en cas de si 1 falla tenir l'altre amb les mateixes dades


![](assets/raid_35.png)


**Elimina el volum i torna'l a crear agafant 25 GB a cada disc.**

Per fer això hem de anar a "administrador de discos" > clic dret en "Disco 1" > Eliminar Volumen >

Ara fem clic dret en "Disco 1" > nuevo volumen reflejado > En la primera finestra seleccionem el disc 1 i 2 i li posem 25GB

Si ho hem fet be ens tindria que quedar així

![](assets/raid_36.png)

**Anar a inicio->Equipo per comprovar que tenim dos discos (C i E). Quin tamany te ara E?**

Te de 24,3 GB


![](assets/raid_37.png)

**Entra al volum E i crea una estructura de directoris amb 3 nivells i arxius a dintre de prova.**

![](assets/raid_38.png)

**Trenca el mirall.**

Per fer aixó anem al disc 1 fem clic dret i escollim l'opció "Quitar reflejo"

![](assets/raid_40.png)

**Anar a inicio->Equipo i comprovar quants discos tenim i les seves lletres d'unitat. Quin tamany te cadascun? Contenen dades? Quines? Ara ja no hi ha redundància.**

![](assets/raid_41.png)

![](assets/raid_42.png)

**Per refer el mirall, afegeix al volum E un mirall amb el disc 3 (no deixa escollir el disc 2, ja que s'està fent servir en un disc normal i te dades).**

Per fer aixó anem a "administrador de discos" > Clic dret al volum E > "Agregar reflejo" > Disco 3

![](assets/raid_43.png)

![](assets/raid_44.png)

Observa que Windows reconstrueix el mirall ("volviendo a sincronizar"), i triga bastant (en el teu cas, quant?).

![](assets/raid_45.png)

En el meu cas aproximadament 5 minuts

**Ara simulem que un dels discos falla. Apaga l'equip abruptament (apagar màquina virtual tancant-la "malament") i elimina el disc 3 de la controladora SATA de l'equip virtual (a opcions de la màquina virtual).**

Per eliminar el disc hem d'anar a Paràmetres de la maquina virtual > emmagatzematge > seleccionar el disc3 > Clic dret > eliminar adjunció

![](assets/raid_46.png)

**Torna a engegar l'equip i a obrir el gestor d'emmagatzematge. Quin missatge tenim respecte el disc3? Es pot accedir al volum E?**

![](assets/raid_50.png)

Si que podem accedir

**Cal trencar el mirall per tenir el volum E operatiu? I en aquest cas, podem accedir a les dades que s'hi guarden?**

No, en aquest cas si

**Busca evidències als logs del sistema respecte als discos en mirall (creació, fallada, etc.)**


![](assets/raid_52.png)

![](assets/raid_51.png)

**Es pot crear un RAID 1 amb dos discos de tamanys diferents? Comprova-ho**

Per fer-ho primer he anat a "administrador de discos" > clic dret disc 1 > nuevo volumen rejleado > en l'assistent he afegit els altres 25 GB del disc 2 i al afegir-ho el tamany dels 2 disc m'ha canviat a 25 GB per tant no es pot crear un RAID 1 amb dos discos de tamany diferent

![](assets/raid_53.png)

![](assets/raid_54.png)

### RAID 5 (Bandes amb paritat): Anem a crear un volum amb bandes amb paritat agafant 3 discos i simular fallades

**Eliminar volums de l'exercici anterior i torna a afegir el disc 3 a la controladora SATA de l'equip virtual (quan l'estas connectant, no tornis a crear un disc nou, fes servir el que ja hi havia. Tindrem els espais sense assignar (si no, esborrar els volums).**

![](assets/raid_55.png)

**Crea el volum RAID 5 amb els 3 discos, assigna la unitat Z i posa-hi dades (carpetes i fitxers, petits).**

Per fer això hem d'anar a "administrador de discos" > clic dret disc1 > nuevo volumen RAID 5 > en la primera finestra de l'assistent Afegim el 3 discos amb l'opció "Afegir"

![](assets/raid_56.png)

En aquesta finestra canviem la lletra E per Z després en les altres finestres deixem les opcions per defecte


![](assets/raid_57.png)

**Comprova quantes unitats tenim. Quin tamany tenen els volums que es veuen a "Equipo"? Quant d'espai hem perdut? Per què?**

**Simula una fallada del sistema. Apaga l'equip abruptament (apagar màquina virtual tancant-la amb el botó "power") i elimina el disc 3 de la controladora SATA de l'equip virtual (a opcions de la màquina virtual).
Torna a engegar l'equip i a obrir el gestor d'emmagatzematge. Quin missatge tenim respecte el disc3? Es pot accedir a les dades al volum RAID 5?
Si has pogut accedir a les dades, modifica el contingut d'alguns fitxers.
Reconstrueix el volum RAID 5 tornant a apagar l'equip i afegint de nou el disc 3. Què passa amb els fitxers que has modificat?
Busca evidències als logs del sistema respecte als discos RAID 5 (creació, fallada, etc.)**
