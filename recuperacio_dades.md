# RECUPERAR DADES AMB TESTDISK I DESTRUCCIÓ SEGURA

**En un Ubuntu Desktop afegiu la imatge Disk1-cop.vdi (imatge de disc VirtualBox)**

Per afegir la imatge ens anem a la maquina virtual a la opció de emmagatzematge i despres fem clic a l'opció del disc i despres a "tria un disc existent"

![](assets/dadess.png)

Una vegada fet això ens anem on està el disc en aquest cas està en "baixades" i el seleccionem

![](assets/dades_2.png)

Llavors s'afegira si ho hem fet be ens tindria que quedar així

![](assets/dades_4.png)

**Mireu la informació de particions amb fdisk**

Per fer això hem d'executar la següent comanda
~~~
sudo fdisk -l /dev/sdb
[sudo] contraseña para saguilar:
Disco /dev/sdb: 10 GiB, 10737418240 bytes, 20971520 sectores
Unidades: sectores de 1 * 512 = 512 bytes
Tamaño de sector (lógico/físico): 512 bytes / 512 bytes
Tamaño de E/S (mínimo/óptimo): 512 bytes / 512 bytes
Tipo de etiqueta de disco: dos
Identificador del disco: 0x1bb4858a
~~~

**Comproveu si es pot muntar el disc manualment (feu servir mount).**

En aquest cas no ens deixa montar el disc dur, perque no hem donat format al disc

~~~
media$ sudo mount /dev/sdb /media/saguilar/
mount: /media/saguilar: tipo de sistema de ficheros incorrecto, opción incorrecta, superbloque incorrecto en /dev/sdb, falta la página de códigos o el programa auxiliar, o algún otro error.
~~~

Per muntar el disc primer hem de donar-li format una vegada fet aixó executem aquesta comanda per muntar-lo manualment

~~~
sudo mount /dev/sdb1 /media/saguilar/
~~~


**Feu una imatge binària del disc amb dd. Guardeu la imatge en un fitxer anomenat imatge-binaria.img. Ara tindrem una imatge del disc dur idèntica bit a bit (del disc dur, no de la imatge de disc virtual que us he passat. Si teniu dubtes, comproveu el hash i compareu).**

Per fer aixo executem aquesta comanda
~~~
sudo dd if=/dev/sdb of=Escritorio/imatge-binaria.img
20971520+0 registros leídos
20971520+0 registros escritos
10737418240 bytes (11 GB, 10 GiB) copied, 111,832 s, 96,0 MB/s
~~~

**Instal·leu el programa testdisk, executeu testdisk com a root. En iniciar, cal crear un nou fitxer de logs.**

Per fer aixó primer executem aquesta comanda

~~~
sudo apt install testdisk
~~~

Ara en aquí li escollim la primera opció per crear el fitxer de logs, una vegada fet aixo et creara el fitxer automaticamnt

![](assets/dades_5.png)

**Escolliu el dispositiu sobre el que voleu treballar (el disc muntat) i el tipus de particionat (MBR és Intel PC).**

En aquest cas hem de seleccionar la segona opció

![](assets/dades_21.png)

I després la primera opció

![](assets/dades_22.png)

**Arribarem a les opcions del programa. Les que farem servir seran [Advanced] per a treballar amb fitxers i [Analyse].
Entrem en [Advanced] i amb l'opció [list] mirem si hi ha fitxers.**

Per entrar en l'opció "Advanced" només la hem de seleccionar

![](assets/dades_20.png)

En aquest cas ens diu que la partició no esta disponible

![](assets/dades_24.png)


**Entrem a [Analyse] i mirem la informació de possibles particions. Si en trobeu, recupereu-les i indiqueu quins sistemes d'arxius tenen i les seves característiques.**

Per entrar en aquesta opció hem de anar fins al principi amb la tecla "q", una vegada estiguem al principi seleccionem la opció "Analyse"

![](assets/dades_25.png)


Una vegada fet això abaix seleccionem aquesta opció  "Quick Search "

![](assets/dades_26.png)

En aquest cas ens troba varies particions

![](assets/dades_27.png)

La primera partició es primaria i es en format ext4 es de 4GB

![](assets/dades_40.png)

La segona partició es de FAT32 i es de 4GB te un directori prova que a dins hi han aquests arxius

![](assets/dades_41.png)

La tercera partició es NTFS es de 4GB  i hi han aquests arxius

![](assets/dades_43.png)

Per recuperar aquestes particions nomes hem de seleccionar la opció "write"

**Entrem en [Advanced] i mirem ara de nou si hi ha fitxers a recuperar.**

En aquest cas hi esten les particions i a dins hi han fitxers

![](assets/dades_45.png)

**Si trobem fitxers esborrats, els recuperem. Indiqueu quins fitxers hi ha i en quines particions es troben.**

Per fer aixó ens anem a la opció "Advanced" i en aquest cas en la primera partició no hi ha res pero en la segona si doncs ens anem a la segona partició al directori prova i anem a una imatge

![](assets/dades_46.png)

Seleccionem les imatges i li donem a l'opció "c" que et copia les imatges una vegada fet aixo en aquet cas jo he creat una carpeta en l'escriptori on guardaré les imatges, doncs me he anat fins a aquella carpeta i la imatge s'ha copiat

![](assets/dades_47.png)

Llavors he fet lo mateix amb la tercera partició

![](assets/dades_48.png)

**Provem a muntar el disc al sistema un cop recuperades les dades. Es pot accedir amb Nautilus? Hi ha els fitxers que heu recuperat a dintre?**

Per fer aixó hem de fer aquestes comandes

~~~
saguilar@saguilar-VirtualBox:~$ sudo mount /dev/sdb1 /media/saguilar/
saguilar@saguilar-VirtualBox:~$ sudo mount /dev/sdb2 /media/saguilar/
saguilar@saguilar-VirtualBox:~$ sudo mount /dev/sdb3 /media/saguilar/
~~~

Si que podem accedir, però els fitxers no estan

![](assets/dades_49.png)

## Photorec i Foremost

1. Ara amb la imatge imatge-binaria.img, proveu de recuperar les dades fent servir Testdisc (poseu la imatge com a paràmetre) i la seva aplicació germana, Photorec.

Per fer això primer obrim un terminal i anem a Escriptori

![](assets/dades_50.png)

Ara executem aquesta comanda

~~~
saguilar@saguilar-VirtualBox:~/Escritorio$ photorec imatge-binaria.img
~~~

S'obrira una finestra i escollim la imatge

![](assets/dades_53.png)

Llavors en aquesta finestra li donem a la opció de "Search"

![](assets/dades_54.png)

Aquí escollim la primera opció

![](assets/dades_55.png)

I en aquí escollim la opció  "c" que es per dir-li el directori on guardara la recuperació

![](assets/dades_56.png)

**Comprovació**

![](assets/dades_57.png)

**Fes el procés utilitzant l'eina de recuperació Foremost, amb imatge-binaria.img, i extreu tots els fitxers que hi hagi a la imatge de disc.**

Per fer això executem aquesta comanda

~~~
sudo foremost -T all -i imatge-binaria.img -o /home/saguilar/Escritorio/foremost

~~~

**Quina diferència trobes entre Foremost, Testdisk i Photorec? Explica-ho**

La diferencia que jo veig es que Testdisk et dona moltes mes posibilitats que Foremost i Photorec

**Amb l'utlilitat kpartx, intenta muntar les particions que es trobi a dintre el fitxer binari.**
Per fer això executem aquesta comanda

~~~
sudo kpartx -l -a imatge-binaria.img
~~~


## Destrucció de dades amb seguretat

**Sembla que la policia truca a la porta. Munteu el disk anterior (Disk1-cop.vdi) i destruïu les proves amb alguna eina que destrueixi les dades completament (dd o wipe per exemple).**

Per fer-ho amb wipe es aquesta comanda

~~~
sudo wipe -kqD /dev/sdb*

~~~



**Intenteu recuperar les dades amb testdisk i foremost. Assegureu-vos que ja no es pot. Mostreu evidències.**

En aquest cas si anem a "Advanced" amb testdisk no ens apareix cap partició

![](assets/dades_60.png)

I si posem aquesta comanda amb "foremost" ens surt el seguent error

~~~

sudo foremost -T all -i /dev/sdb* -o /home/saguilar/Escritorio/foremost
sudo foremost -T all -i /dev/sdb* -o /home/saguilar/Escritorio/foremost

~~~
