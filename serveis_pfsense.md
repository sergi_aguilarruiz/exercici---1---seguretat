
# Opció Avançada Pfsense

**1- Maquina virtual amb les dues interficies**

![](assets/sr-1.png)

![](assets/sr-2.png)

**2- Client**

![](assets/sr-3.png)

**3- Configuració Pfsense**

![](assets/sr-4.png)

**4- Squid en mode transparent**

![](assets/sr-5.png)

## SQUID

### Instal·lació

Per instal·lar el squid anem a "System - Package Manager- Availabe Packages " i busquem "squid" i instal·lem el paquet que diu "squid" quan estigui descarregat ens tindria que aparèixer aquí.

![](assets/sr-6.png)

Ara anem a "Services - Squid Proxy Server"

![](assets/sr-12.png)


I la configuració general l'hem de deixar així.

![](assets/sr-7.png)

![](assets/sr-8.png)

![](assets/sr-9.png)

![](assets/sr-10.png)

Quan ja tinguem la configuració general com les captures ho guardem.

### Comprovació

Per comprovar les regles he utilitzat l'extensió "FoxyProxy".

Configuració Proxy en la xarxa WAN

![](assets/sr-14.png)

Configuració Proxy en la xarxa LAN

![](assets/sr-15.png)

### Regles

**1- Nomes navegar des de la LAN**

Si accedim des de la maquina virtual client a qualsevol web ens posa això

![](assets/sr-16.png)

Anem a la opció "ACL", posem la IP de la interfície LAN i ho guardem.

![](assets/sr-13.png)

Llavors comprovem que ens deixa accedir des de la interfície LAN

![](assets/sr-17.png)

Per últim comprovem que no ens deixa accedir des de la interfície WAN.

![](assets/sr-19.png)

**2- No permetre IP origen concreta**

Anem a la opció "ACL" i despres en "Banned Hosts Addresses" posem la IP que volem bloquejar, en aquest cas he posat la meva que es la 192.168.2.5.

![](assets/sr-25ç.png)

Comprovació

Primer me he canviat la IP manualment y m'he posat la 2.10, he probat de anar a "Facebook" i m'ha deixat.

![](assets/sr-23.png)

Ara m'he tornat a posar per DHCP y m'ha donat la que tenia que es la 2.5

![](assets/sr-24.png)

**3- Bloquejar 2 sites concretes**

Anem a la opció "ACL" i despres a "BlackList" llavors posem el domini que volem blocar, en aquest cas he blocat el de Facebook i el de marca..

![](assets/sr-26.png)

Comprovació

![](assets/sr-28.png)

![](assets/sr-29.png)

**4-Bloquejar navegador chrome**

Hem d'anar a "ACL" i despres a "Block User Agents" i posem "Chrome"

![](assets/sr-30.png)

Comprovació

Per comprovar-ho m'he instal·lat la extensió "FoxyProxy" en chrome. Quan he intentat accedir a qualsevol web amb dona aquest error.

Entra al moodle amb el proxy per defecte.

![](assets/sr-31.png)

Entra al moodle amb el meu proxy.

![](assets/sr-32.png)

**5-Demanar usuari i password**

Anar a la configuració general i desactivar el mode transparent perquè no et deixa activar la autenticació si tens activada aquesta opció.

![](assets/sr-33.png)

Ara anem a "Authentication" i en la primera opció escollim "RADIUS", en la segona posem la IP del proxy, la tercera el port i en l'ultima posem una contrasenya en aquest cas es "P@ssw0rd"

![](assets/sr-40.png)

![](assets/sr-41.png)

Ara instal·lem el paquet FreeRADIUS.

![](assets/sr-42.png)

Ara anem a "Services" "FreeRADIUS"

![](assets/sr-43.png)

I fem un usuari, en aquest cas el "username" es "sergi" i la contrasenya es "futbol123".

![](assets/sr-44.png)

Només hem de modificar aquests paràmetres.

![](assets/sr-45.png)

Si ho hem fet be ens tindria que apareixer.

![](assets/sr-46.png)

Ara anem a la opció "NAS/Clients" i fem clic a la opció "Add"

![](assets/sr-47.png)

En "Client IP Address" posem la IP del router.

![](assets/sr-48.png)

En Client Shortname posem "Pfsense"

![](assets/sr-49.png)

En "Client Shared Secret" posem la contrasenya que hem creat abans en aquest cas es "P@ssw0rd" i ho guardem.

![](assets/sr-50.png)

Si ho hem fet be ens tindria que aparèixer aquí.

![](assets/sr-51.png)

Ara anem a la opció "interfaces" i afegim una.

![](assets/sr-52.png)

En aquest cas nomes hem de modificar l'opció de "Port" per el 3128.

![](assets/sr-53.png)

![](assets/sr-54.png)

Comprovació

Busquem alguna pagina sense posar l'usuari ni la contrasenya i ens posa aquest missatge.

![](assets/sr-55.png)

Llavors anem al FoxyProxy i afegim l'usuari que hem creat.

![](assets/sr-56.png)

Ara si fem una cerca no ens tindria que donar cap problema.

![](assets/sr-57.png)

Per no fer servir l'autenticació per una IP en concret anem a "ACL" i en l'opció "Unrestricted IPs" posem la IP en aquest cas es la meva.

![](assets/sr-58.png)

Comprovació

He tret el usuari del FoxyProxyi he provat a fer una cerca.

![](assets/sr-59.png)

![](assets/SR-60.png)

### SQUIDGUARD

Primer hem de instal·lar el paquet, per fer-ho anem a "System - Package Manager" i busquem "SQUIDGUARD" i ho instal·lem.

![](assets/sr-60.png)

Si ho hem fet be en "Services" ens tindria que apareixer.

![](assets/sr-61.png)

Abans de començar hem d'activar-ho per fer-ho anem a "Services - SQUIDGUARD - General Options" i marquem aquesta Opció

![](assets/sr-62.png)

Li donem a "Apply" i ens sortira que està activat

![](assets/sr-63.png)

**1-Bloquejar categoria violència i que et surti un missatge personalitzat**

Dins del SQUIDGUARD anem a "Target categories" escollim l'opció "ADD". En "Name" posem el nom de la regla en aquest cas es aquest.

![](assets/sr-64.png)

L'opció order la deixem predeterminadament, en l'opció "Domain List" posem en aquest cas dominis que tenen que veure amb violencia, en aquest cas he posat el de "ufc"


![](assets/sr-65.png)

En "Redirect mode" escollim aquesta opció.

![](assets/sr-67.png)

Llavors posem una descripció i marquem la opció dels logs. I ho guardem, si ho hem fet be ens tindria que aparèixer aquí.

![](assets/sr-68.png)

![](assets/sr-69.png)

Ara anem a "Common ACL", i fem clic al "+".

![](assets/sr-70.png)

I en la ACL que hem creat en "access" escollim l'opció "deny"

![](assets/sr-71.png)

En la opció "Default access" escollim "allow".

![](assets/sr-72.png)

Anem a la opció "Proxy Denied Error" i posem el missatge personalitzar que sortira quan un usuari intenti accedir a una pagina que estigui blocada.

![](assets/sr-73.png)

Per ultim en l'opció "Redirect Mode" escollim "int error page (enter error message)", ho guardem i en "General Settings" li donem al boto verd.

![](assets/sr-74.png)

Comprovació

Intentem entrar a la pagina i ens surtira el missatge personalitzat.

![](assets/sr-75.png)

### Lightsquid

Primer hem de instal·lar el paquet, per fer-ho anem a "System - Package Manager" i busquem "Lightsquid" i ho instal·lem.

![](assets/sr-76.png)

Ara anem a "Status - Squid Proxy Reports" i ho configurem tal que així.

En el primer apartat només he canviat la contrasenya per "P@ssw0rd"

![](assets/sr-77.png)

EL segon apartat es per modificar la part visual i el idioma, en aquest cas només he modificat la part visual.

![](assets/sr-78.png)

En aquest apartat he modificat el temps de actualització, en aquest cas cada 10 minuts. Fet això ho guardem

![](assets/sr-79.png)

**1- S'han de mostrar estadístiques d'ús del proxy**

Per fer això fem clic en "Open Lightsquid" i ens apareix quins usuaris s'han conectat, que temps, tambe podem veure-ho amb grafics.

![](assets/sr-80.png)
