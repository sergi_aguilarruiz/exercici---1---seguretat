## Eines de gestió de contrasenyes

### Manual Keepassx

### **Menú general**
**Database**
Aquesta opció es per crear una nova base de dades, per importar,
per veure el historial de la base de dades, per obrir la teva base de dades,
o per tancar el programa.

![](assets/kee-1.png)

**Entries**
Aquesta opció es per afegir noves contrasenyes a la teva base de dades, per
modificar-les, per crear noves contrasenyes per a que siguin mes segures, per
esborrar les contrasenyes que estén en la base de dades o per copiar algun
password o username d'alguna entrada.

![](assets/kee-2.png)

**Groups**
Aquesta opció serveix per afegir i eliminar grups a la teva base de dades, quan
fas un grup en aquest grup pots afegir contrasenyes, per exemple això podria
servir per crear un grup, per només posar les contrasenyes de les coses del
cole.

![](assets/kee-3.png)

**Wiew**
Aquesta opció simplement es per escollir si vols veure o no la barra d'Eines

![](assets/kee-8.png)

**Tools**
Aquesta opció es per bloquejar les base de dades i per anar a la configuració
general.

![](assets/kee-9.png)

**Help**
Aquesta opció es per si tens alguna duda o algun problema.

![](assets/kee-10.png)

### Creació d'entrades

**Gmail**

![](assets/kee-11.png)

**Moodle**

![](assets/kee-12.png)

**App Inventor**

![](assets/kee-13.png)

**Imagine**

![](assets/kee-14ç.png)

### Com accedir a les Webs des-de el Keepassx

Primer fem clic dret sobre el registre i escollim l'opció **"Open URL"**

![](assets/kee-15.png)

Una vegada estiguem en la pagina anem al Keepassx i  fem clic dret sobre el
registre i escollim l'opció **"Copy Username**" i **"Copy Password"**

![](assets/kee-16.png)

 Per últim copiem el usuari i al contrasenya copiada a la pagina i iniciem la
 sessió

 ![](assets/kee-17.png)

 ### Exportar la BD i utilitzar-la en Windows

 Per Exportar anem a l'opció "Database" i escollim l'opció "Export to CVS file"

 ![](assets/kee-18.png)

 Ara escollim la ruta en aquest cas he guardat la BD en l'escriptori

  ![](assets/kee-19.png)


  Una vegada fet aixo, fem una carpeta compartida en el VirtualBox, per fer-la
  anem a la maquina, a els paràmetres, i fem clic a l'opció que esta
  a la dreta

  ![](assets/kee-20.png)

  Després escollim la ruta de la carpeta compartida i ja la tindrem.

  Una vegada fet aixo, he iniciat la meva maquina virtual amb Windows 10 i he
  baixat el **Keepassx**

  ![](assets/kee-21.png)

  Una vegada me l'he descarregat, he executat al kepass i he escollit l'opció
  de importar la base de dades

![](assets/kee-22.png)

Llavors he copiat la base de dades des de  la carpeta compartida a l'escriptori,
de la maquina virtual i he importat la base de dades

![](assets/kee-26.png)

### Resultat

![](assets/kee-27.png)
