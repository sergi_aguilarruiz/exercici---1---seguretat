# Preguntes

## 1 Fer còpia:

**Quins tipus de dades anem a copiar?**

Anem a copiar base de dades, arxius de configuració del wordpress i arxius mysql.

**Quines eines de còpia utilitzarem segons les dades?**

Per la base de dades utilitzarem **mysqldump**

Per el wordpress faré un script que et creara la copia de seguretat local i després utilitzaré la eina     

**Què copiem?**

Copiem la carpeta /var/www/html/wordpress que conte el wordpress

Copiem la base de dades sergi que es la que utilitzo en el wordpress

**Què no copiem?**

Les demès bases de dades i els demes arxius de /var/www/html  

**Amb quina freqüència copiem?**

Cada Dilluns

**En quin moment del dia copiem?**

A les 12 del mati

**Còpia manual o automàtica? (qui ho fa o quin sistema automàtic)?**

Automàtica i per la base de dades les fa mysqldump amb un script i per el WordPress les fa amb un script en local i en el nuvol les fare amb




**Qui fa les còpies, les
les transporta?
Quant (GB) ocupen les dades que copiem?**

Si, els arxius del wordpress ocupen 44M.

**Quant ocupa (GB) el total de còpies que guardem?

5 GB

Quant de temps disposem/necessitem per a fer la còpia?**

3 dies

**Quins suports fem servir?
Tenen data de caducitat els suports?**

Farem servir el servidor i el nuvol.

No, perque es van actualitzant


**Quan es restauri existiran els dispositius de lectura?**

Si


**On guardem físicament els suports?**

En els servidors

**Cal xifrar les dades?**

Si

**Quin tipus de còpia fem? (completa, diferencial, incremental)**

Completa

**Com sabem si s'ha fet bé la còpia?**

Fem un hash o comprovant restaurar-la

**Com es registra en documents les còpies fetes?**

La documentació de les copies les faré en un arxiu md que pujaré al gitlab.


**Fins quan seran vàlides les còpies?**

Duraran 7 dies les copies.

## 2 Restaurar còpia:

**Quins tipus de dades anem a restaurar.**

Anem a restaurar base de dades, arxius de configuració del wordpress i arxius mysql.


**Com sabem que el backup està bé abans de restaurar?**

Amb un hash

**Qui pot restaurar?**

L'administrador

**On restaurem?**

En una maquina virtual nova

**Com es restaura la còpia?**

Es restaura posant els fitxers en una maquina virtual nova.

**Podem restaurar parcialment o ha de ser total?**

Total

**Quant trigarem a restaurar?**

Com a màxim 3h


## Restauració

Primer fem una imatge del sistema ho podem fer amb el clonezilla.

Per provar que la copia funciona primer exportem la base de dades del wordpress al nostre directori actual en aquest cas es "/home/sergi. Primer hem de instal.lar el mysqldump amb aquesta comanda

~~~
sudo apt-get install mysql-client
~~~

Ara executem aquesta comanda per exportar la base de dades en aquest cas el nom de la DB es "Sergi" i l'arxiu que farà serà amb el següent nom **"sergi.sql"**

~~~
mysqldump -u root -p sergi > sergi.sql
~~~

Ara executem el script i despres executem la comanda **"scp"** per copiar els arxius en una altre maquina.

~~~
scp -r /home/sergi/copia sergi@172.16.100.192:/home/sergi
~~~

## Script de Backup

Aquest script basicament el que fa es que t'importa la database del wordpress a un directori amb un arxiu .sql i despres et copia els arxius del wordpress i els mou en un altre direcori al complet

Al final el script m'ha quedat així

![](assets/back_2.png)

### Comprovació 

![](assets/back_3.png)
