# Permisos drets i logs

Primer fem 3  usuaris sense privilegis, per fer-los anem  a "Administración
de dispositivos", "Usuarios i grupos locales", "Usuarios" i en aquesta carpeta
fem 3 usuaris

![](assets/usr-1.png)

## Opcions de seguretat i drets del sistema

Per millorar la seguretat del nostre sistema Windows, hem d'anar a les opcions
de seguretat de les directives locals , per anar hem d'anar a "Inició"
i buscar "Directivas de seguridad local", anem a la carpeta "Directivas Locales"
i per últim a "Opciones de seguridad"

![](assets/seg-2.png)

### Configuració

**No mostrar el ultimo inició de sesión"**

Aquesta opció es per que no es mostri el nom d'usuari que va iniciar la sessió
per ultima vegada, per tenir aquesta opció nomes l'habilitem

![](assets/seg-3.png)

**"Cambiar el nombre de la cuenta de Administrador"**

Aquesta opció es per canviar el nom del compte d'administrador, així si una
persona volgués entrar com administrador li seria mes difícil accedir ja que el
nom estaria canviat. En aquest cas posarem com a nom "Amagat"

![](assets/seg-4.png)

**"Titulo de mensaje para los usuarios que intentan iniciar sesión"**

Aquesta opció es per que surti un títol al iniciar la sessió en aquest cas el
títol serà **"Atenció"**

![](assets/seg-5.png)

**"Texto de mensaje para los usuarios que intentan iniciar sesión"**

Aquesta opció es per que surti un text al iniciar la sessió, en aquest cas el
text sera "Equip destinat a empleats autoritzats"

![](assets/seg-6.png)

### Drets d'usuaris sobre el sistema

Primer anem a la opció per poder  apagar el sistema, per fer això hem d'anar a
la carpeta "Asignación de derechos de usuario" i després a la opció
"**Apagar el sistema**". Una vegada estiguem en l'opció afegim a
l'usuari **"Joan" i "Albert"** i trèiem el grup **"Usuarios"**.
Si hem fet be tot això ens tindria que quedar així:

![](assets/seg-7.png)

Per últim tenim que habilitar a el Joan i l'Albert que puguin canviar l'hora del
sistema, i també hem de treure el grup "Usuarios" d'aquesta opció. Per poder
fer aixó primer anem a l'opció **"Cambiar la hora del sistema"**, i afegim
als usuaris **"Joan i Albert"**, i treiem el grup **"Usuarios"** si ho hem fet
be ens tindria que quedar així

![](assets/seg-8.png)

## Permisos al sistema d'arxius

### Veure la ACL d'un arxiu o una carpeta

Per poder visualitzar la ACL d'un arxiu o una carpeta hem de fer clic dret en
aquest arxiu o carpeta, anem a les "Propiedades", i "Seguridad", a dalt
podem veure els usuaris i grups i abaix podem modificar els permisos.

![](assets/seg-9.png)

### Configuració

Primer creem dues carpetes a la unitat c:\ aquestes carpetes es diran
**"Alumnes"** i **"Profes"** per crear aquestes carpetes fem clic dret "Nuevo",
"Carpeta", si ho hem fet be tindria que quedar com aixó

 ![](assets/seg-10.png)

 Hem de crear dos grups, per fer-los fem clic dret al icona de "Windows", i
 escollim l'opció "Administrador de equipos"

 ![](assets/seg-11.png)

 Ara anem a l'opció "Usuarios i grupos locales", "Usuarios" i fem els seguents
 usuaris:

**-Roger**

**-Maria**

**-Pol**

per fer aquests usuaris fem clic dret "Usuario nuevo", si hem fet be els usuaris
ens tindria que quedar així:

![](assets/seg-12.png)

### Creació de grups

Ara fem dos grups, per fer-los anem a la carpeta **"Grupos"**, quan estiguem a
la carpeta fem clic dret "Grupo Nuevo". Els grups es diran
**"gPROFES i gALUMNES"**

![](assets/seg-13.png)

Ara anem a les propietats de la carpeta "Alumnes", després a "Seguretat"
i afegim el grup "gALUMNES" en aquesta carpeta, per afegir-lo fem clic a la opció
"Editar", busquem el grup i automàticament el grup s'afegira, ara només marquem
la opció de "Lectura" i "Escriptura" per el grup "gALUMNES" en aquesta Carpeta.

![](assets/SEG-14.png)

### Modificar els permisos dels grups

Trèiem el grup **"Usuarios",** per fer-ho anem a "Opciones Avanzadas"
"Cambiar permisos"
"Incluir todos los permisos heredables del objeto primario de este objeto".
Fem el mateix amb el grup de "**Usuarios identificados"**. Això ho fem per
 treure l'herència dels grups amb aquesta Carpeta, després li donem a la opció
 "Quitar".

Ara afegim el grup "gPROFES" a la carpeta "Alumnes" i li donem drets de Lectura
per fer aixo fem clic a l'opció "Agregar" i busquem el grup, després nomes
marquem l'opció de "Lectura" i li donem a "Aplicar", i afegim el grup "gALUMNES"
a la carpeta "Alumnes" i li donem drets de lectura i escriptura.

![](assets/seg-15.png)


Per ultim afegim el grup **gPROFES** a la carpeta **"Profes"**
i li donem permisos de lectura i escriptura, per fer-ho anem a la carpeta
seleccionem l'opció "Agregar" busquem el grup, s'afegira sol per ultim marquem
només les opcions de lectura, escriptura i li donem a "Aplicar"

![](assets/seg*16.png)


### Afegir usuaris al grup
**Grup gPROFES**
Per afegir un usuari a un primer anem al grup, fem clic dret anem a les seves
propietats, anem a l'opció "Miembros", escollim l'opció "Agregar", i en aquest
cas busquem l'usuari "Roger", afegim al usuari i li donem a "Aplicar"

![](assets/seg-17.png)

**Grup gALUMNES**

Repeteim els mateixos pasos que l'anterior vegada pero en aquest cas amb
l'usuari "Maria", "Pol". Fet tot aixo, aquest seria el resultat.

![](assets/seg-18.png)

### Permisos efectius de Roger, Maria i Pol

En aquest cas els permisos efectius de Roger sobre la carpeta Alumnes son de
lectura només, perquè com ell pertany al grup "gPROFES", i aquest només te  
el permís de lectura doncs l'usuari hereta aquests permisos

Els permisos efectius de Maria i Pol en aquest cas son de Lectura i escriptura,
perquè pertanyen al grup de "gALUMNES" i aquest grup te els permisos de Lectura
i escriptura en la carpeta Alumnes.

Quan he denegat els permisos de  escriptura a Maria en la carpeta
"Alumnes", el grup "gALUMNES" ha sigut  mantenint els permisos de Lectura
i escriptura.

Els permisos efectius de Roger sobre la carpeta "Profes" son de lectura i
escriptura.

He comprovat que roger es el propietari anant a les "opciones avanzadas",
de seguretat i mirant el propietari.

Els permisos efectius de roger han augmentat, en aquest cas, com es el
propietari te el control total

Quan he creat directoris amb Roger a la carpeta "Alumnes" m'ha demanat la
contrasenya d'administrador, i a la carpeta "Profes" m'ha deixat.

Quan he iniciat sessió amb Pol he anat a modificar l'arxiu creat abans per
maria i m'ha deixat, perquè com Pol està dins del grup "gALUMNES" i per tant
hereta els permisos.

## Registre de successos o Logs

Per anar al registres del sistema, primer anem a "Panel de control", "Sistema y
Seguridad", "Herramientas administrativas", i per ultim "Visor de eventos"

![](assets/seg-19.png)

**"Registros de Windows - Aplicación - Registre drefag"**

Aquest registre informa sobre l'arrencada del sistema.

**"Registros de Windows - Seguridad "**

Prova - Inicar sessió amb una contrasenya incorrecta i despres la correcta.

![](assets/seg-29.png)

![](assets/seg-21.png)

Prova - Canviar l'hora del sistema amb l'usuari Joan i Albert

![](assets/seg-22.png)

![](assets/seg-23.png)

Prova - Crear un usuari

![](assets/seg-25.png)
