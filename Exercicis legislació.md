# Exercici legislació

### Quadre resum

Delictes informatics  | Definició  |  Exemples |  
-|||
Delicte d'intrusió informàtica  | Entrada al conjunt o part d'un sistema d'informació vulnerant les museres de seguretat i no tenint autorització |  Entra a un sistema a traves de un port obert, Desxifrar un disc dur i entra en ell...  |  
Interceptació de transmissions de dades informàtics   | Interceptar transmissions informàtiques que no son publiques  | Sniffing, keyloogers...  |  
Amenaces  |  Amençar a una persona a traves d'un mitja de comunicació | Amençar al president de Vox per twitter, ameneçar al rei per facebook  |  
Delictes contra l'honor: Calumnies i injuries  | Calumnia: Acusació falsa a una persona. Injuria: Fer malbé la integritat o l'honor d'una persona  | Acusar a un politic de robar per youtube, fer un rap desprestigiant la corona  |  
Fraus informátics  |  Enganyar a gent amb anim de lucre i fent alguna cosa que sigui perjudicial per ell/a | Phising bancari, ONG falsa...  |  
Sabotatge informàtic | Interrompre el funcionament d'un sistema informatic, crear o utilitzar programes per cometre delictes i esborrar dades o programes.  |  DDOS, spoofing... |
Pornografia infantil | Fer, vendre o distribuir pornografia de menors  | Que la teva novia de 17 anys t'envia una foto sense roba i tu la puguis a una pagina pornogràfica, vendre un video pornografic de menors  |
Falsificació | Fer una cosa falsa i pasar-la per una de veritat  |   | Vendre una camiseta falsa, vendre unes bambes falses ...   |  



### Protecció de dades de caràcter personal


**Què volen dir les sigles RGPD?**

Reglamento General de Protección de Datos

**Quina motivació té la RGPD?**

Preservar els drets i llibertats de les persones


El tractament de dades personals ha de estar concebut per servir a la humanitat.

Les persones han de tenir el control de les seves dades personals

Aconseguir una regulació mes uniforme

Garantir la lliure circulació de dades.

Reforçar la seguretat jurídica i generar la confiança que permeti a l'economia digital desenvolupar-se en tot el mercat

**Per què s'ha redactat la LOPD? Què volen dir les sigles?**

Ley Organica de Protección de Datos Personales y garantía de los derechos digitales,
aquesta llei s'ha redactat per adaptar-la a la llei d'espanya davant la llei europea i per protegir les dades personals de les persones, i el seu tractament.


**A la web de l'Autoritat Catalana de Protecció de Dades, obté les definicions dels següents conceptes clau i escriu-les amb les teves paraules (NO copiar i enganxar):**



**Dades personals**: Una dada personal es qualsevol informació de la persona, com per exemple el DNI, el nom...
**Fitxer**: Un fitxer es un conjunt de dades personals que estan disponibles i estan estructurades
**Tractament de dades**: Es qualsevol operació que es pugui fer una gravació, conservació, elaboració, modifciació, consulta... de dades.
**Interessat**: Son les persones o titulars de les dades.
**Responsable del tractament**: Es la persona, persona jurídica etc que s'encarrega de utilitzar les dades personals.
**Encarregat del tractament**: Son les persones, persones jurídiques etc que defineixen com s'han de utilitzar les dades personals i amb que.
**Delegat de protecció de dades (DPD)**: Son les persones, o persones jurídiques que son responsables de la protecció de dades de les empreses
**Consentiment de l'interessat**: Es la aceptació de la persona per utiltizar les seves dades personals
**Drets de l'interessat**: Els drets que te la persona sobre el recull de les seves dades personals.
**Violació de seguretat**: Perdua/destrucció intencionada o accidentalment de les dades.
**Dades de consideració especial i tipus.**: Son les dades que es consideren especial davant les dades personals, aquestes son: Origen ètcin o racial, opinions politiques i les conviccions religioses o filosófiques, afiliació sindical, dades genètiques, dades biomètriques, dades relatives a la salut, dades de la vida sexual o les orientacións sexuals.
**Anàlisi de riscos**: Avaluació de riscos per garantir el dret fundamental a la protecció de les dades.




 **Un client nostre que es dedica a la venda de videojocs per internet ens ha demanat que l'orientem respecte la LOPD. Nosaltres buscarem la informació a la web de l'Autoritat Catalana de Protecció de Dades.**


1. Explica al client quines obligacions tindrà com a encarregat del tractament a partir del moment que reculli dades personals dels seus clients.

**Les obligacións son:**

Mantenir un registre de les activitats de tractament.

Determinar les mesures de seguretat aplicables als tractaments que fan

Garantir i estar en condicions de demostrar que el tractament es realitza conforme el RGPD

Designar a un Delegat de protecció de dades

Assistir al responsable en les avaluacions d’impacte i consultes prèvies

Permetre i contribuir a auditories per part del responsable

Contractes d’encàrrec de tractament conclosos amb anterioritat a l’aplicació del RGPD en maig de 2018 han de modificar-se i adaptar-se per respectar aquest contingut,

2. El nostre client ha pensat en demanar dades personals que ara mateix no necessita per que en el futur vol oferir serveis personalitzats als clients. Podrà fer-ho? Per què?

No, perquè segons la llei només pots demanar dades personals quan les necessitis

3. Explica quins drets tindran els seus clients en quant a les dades de caràcter personal.

**Dret d’informació**

Dret de les persones afectades a ser informades en uns aspectes concrets

**Dret d’accés**

El client pot obtenir confirmació de si s’estan tractant les seves dades.

Pot tenir accés a les seves dades i a la informació

El responsable podrà demanar que s’especifiqui la informació de la seva sol·licitud d’accés.

Dret a obtenir una còpia de les dades personals  incloent documents.

Possibilitat de proporcionar un accés remot i segur a les dades.

**Dret a la portabilitat**

El client te  dret a rebre les dades personals que l’afecten

**Dret de supressió**

El client te dret a la  supressió de les dades personals.

**Dret d'Oblit**

El client te dret a que si ha comes alguna cosa  que li pot marcar de per vida tindre la oportunitat de eliminar-ho

**Dret d’Oposició**

EL client te dret a oposar-se a donar les seves dades personals

Dret a no decisions automatitzades

El client te dret a no ser objecte de una decisió automàtica.

**Dret de limitació de tractament**

El client te dret a limitar com es manipulen les seves dades personals



**4. Busca Busca models dels documents necessaris per a informar als clients quan es recullen les seves dades personals. Aquests documents han de estar d'acord a la RGPD.
Cal informar al client sobre els seus drets en el moment de la recollida de les dades.
Cal preveure com podrà exercir el client els seus drets reconeguts al RGPD.**

.El despatx de [nom del col·legiat/da] és el responsable del tractament de les dades dels seus empleats. Gestiona l’expedient personal de cada empleat, efectua els tractaments necessaris per a l’elaboració i abonament de la nòmina i les comunicacions a la Seguretat Social, tracta les dades en compliment de la normativa de salut laboral i pot obtenir també dades per mitjà dels sistemes de control de presència. Tots aquests tractaments es fonamenten en la necessitat de donar compliment a obligacions legals i en el manteniment de la relació contractual.

En relació al tractament de les seves dades cada treballador té els drets següents.

•A  accedir-hi.  Dret  molt  ampli  que  inclou  el  de  saber  amb  precisió  quines  dades  personals  són objecte  de  tractament,  quina  és  la  finalitat  per  la  que  es  tracten,  les  comunicacions  a  altres persones que se’n faran (si és el cas) o el dret a obtenir-ne còpia o a saber el termini previst de conservació. •A demanar-ne la rectificació. És el dret a fer rectificar les dades inexactes que siguin  objecte de tractament per part nostra. •A demanar-ne la supressió. En determinades circumstàncies existeix el dret a demanar la supressió de les dades quan, entre altres motius, ja no siguin necessàries per als fins per als quals van ser recollides i en van justificar el tractament. •A demanar la limitació del tractament. També en determinades circumstàncies es reconeix el dret a demanar la limitació del tractament de les dades.•A  la  portabilitat.  En  els  casos  previstos  a  la  normativa  es  reconeix  el  dret  a  obtenir  les  dades personals pròpies en un format estructurat d'ús comú llegible per màquina, i a transmetre-les a un altre responsable del tractament si així ho decideix la persona interessada. •A  oposar-se  al  tractament.  Una  persona  pot  adduir  motius  relacionats  amb  la  seva  situació particular, motius que comportaran que es deixin de tractar les seves dades en el grau o mesura que  li  pugui  comportar  un  perjudici,  excepte  per  motius  legítims  o  l'exercici  o  defensa  davant reclamacions.

Els drets que acabem d’enumerar es poden exercir adreçant-se directament a [nom del col·legiat/da]. Si no  s’ha  obtingut  resposta  satisfactòria  en  l’exercici  dels  drets  és  possible  presentar  una  reclamació davant  l’Agència  Espanyola  de  Protecció  de  Dades  (www.agpd.es)  per  mitjà  dels  formularis  o  altres canals accessibles des de la seva web.

II.Obligacions i mesures de seguretat a implantar.

Per donar compliment a la normativa de protecció de dades és del tot imprescindible la col·laboració activa de totes les persones que treballen al despatx. És obligatori assumir els principis establerts per aquesta normativa i aplicar les mesures de seguretat que s’indiquen a cada moment. S’ha de garantir que les dades personals que figuren en els documents i sistemes informàtics siguin tractades i utilitzades de forma correcta, en benefici dels clientsi deles altres persones que es relacionen amb el despatx. Per aquests motius cal fer especial atenció a les indicacions següents.

1. Responsabilitat personal. Totes les persones que en el desenvolupament del seu treball tinguin accés a dades de caràcter personal han de complir les instruccions que se li comuniquin. Estan obligades a assumir  com  a  propis  i  aplicar  els  criteris  i  les  mesures  necessàries  per  evitar  l’alteració,  destrucció  i pèrdua  de  les  dades  de  manera  accidental  o  il·lícita,  així  com  el  seu  accés  i  la  seva  comunicació  no autoritzades.

2. Secret professional. Existeix el deure de guardar el secret professional en relació a les dades que es coneguin en el desenvolupament de les tasques pròpies del lloc de treball, obligació que subsisteix uncop finalitzada la relació amb el despatx.

3.  Comunicació  de  dades. Com  a  criteri  general  les  dades  de  caràcter  personal  només  poden  ser comunicades a la persona interessada. La comunicació de les dades a altres persones o a institucions públiques  es  pot  efectuar  únicament  en  els  casos  previstos  en  la  normativa  o  en  compliment  de  les relacions  contractuals  amb  els  clients.  Cal  consultar  els  casos  en  els  que  es  presentin  dubtes  sobre comunicació de dades.

4.  Drets  de  les  persones  de  qui  es  tracten  dades. La  persona  interessada  pot  exercir  els  seus  drets d’accés a les dades, de rectificació, oposició o limitació del tractament i supressió. Es  tracta de drets plenament reconeguts però que requereixen de la identificació prèvia de la persona sol·licitant i seguir determinades formalitats. Excepte autorització prèvia i expressa per atendre aquests drets, correspon al titular  del  despatx  donar-hi  resposta.  Les  sol·licituds  que  es  rebin  se  li  han  de  traslladar  de  maneraimmediata per tal que puguin ser ateses el més aviat possible.

5. Finalitats del tractament. Les dades personals s’utilitzaran únicament per a les finalitats que n’hagin justificat  la  seva  recollida  i  tractament.  No  s’han  de  tractar  de  manera  incompatible  amb  aquestes finalitats. El fet de que estiguin a disposició del despatx no permet fer-ne qualsevol ús. D’altra banda, només poden ser tractades pel personal a qui correspongui segons les responsabilitats assignades i lesfuncions pròpies del lloc de treball.  

6.  Actualització  i  conservació. S’ha  de  procurar  que  les  dades  estiguin  actualitzades.  Cal  adoptar  les mesures raonables perquè es rectifiquin sense dilació les dades personals que siguin inexactes. D’altra banda,  les  dades  s’han  de  conservar  durant  els  terminis  establerts  legalment.  Abans  del  venciment d’aquests terminis a petició de cada persona es poden eliminar les seves dades quan el tractament que se’n  fa  depèn  exclusivament  de  la  seva  voluntat  (per  exemple  dades  de  contacte  en  una  llista  de distribució d’informació).

7.  Lloc  de  treball. Cada  persona  és  responsable  del  lloc  de  treball  des  del  qual  desenvolupa  la  seva activitat laboral, i dels recursos que s’hi hagin destinat. Per aquest motiu ha d’aplicar-hi les mesures de seguretat aprovades i les que, per sentit comú, siguin adequades per garantir la confidencialitat de la informació. En aquest sentit s’ha de garantir que la informació que es mostri a la pantalla de l’ordinador no  sigui  visible  per  a  persones  no  autoritzades,  o adoptar  mesures  per  evitar  que  les  comunicacions verbals  (relacions  presencials  o  comunicacions  telefòniques)  comportin  la  divulgació  d’informació reservada.

8.  Identificadors  personals. Cadascú  és  responsable  de  la  confidencialitat  de  la  contrasenya  o contrasenyes que li permeten accedir als sistemes d’informació i a les dades. No es pot comunicar a una altra persona. L’incompliment d'aquesta obligació seria una falta contra la seguretat. En el cas que una contrasenya arribi a ser coneguda fortuïtament o fraudulentament per altres persones cal comunicar-ho per a poder procedir al seu canvi o anul·lació.

9. Documents en suport paper. En els documents en suport paper figuren dades de caràcter personal. Per  aquest  motiu  cal  adoptar  les  mesures  necessàries  per  a  garantir-ne  la  seguretat,  integritat  i confidencialitat. Cada persona és responsable de la documentació en paper que estigui utilitzant o tingui en custòdia. S’han de guardar de manera que no quedi accessibles a persones no autoritzades. Quan s’hagin  d’eliminar  s’ha  de  fer  de  manera  segura,  evitant  que  les  dades  puguin  ser  conegudes  per persones no autoritzades.

10. Incidències. Qualsevol persona que conegui alguna incidència en relació al tractament de les dades ho  ha  de  notificar  per  a  deixar-ne  constància  en  el  registre  d’incidències.  El  coneixement  i  la  no notificació  d'una  incidència  pot  ser  considerada  una  falta  contra  la  seguretat.  Tenen  la  consideració d’incidència  la  pèrdua  d’un  suport  de  còpia  o  dispositiu  on  figuraven  dades,  el  coneixement  de  la contrasenya personal per part d’una altra persona, l’accés a dades per part de persona no autoritzada,la  troballa  de  documents  amb  dades  personals  en  llocs  que  no  en  garanteixen  la  confidencialitat, l’enviament de correu a llista de destinataris fent visibles les adreces dels destinataris, o altres que hagin afectat o puguin afectar la confidencialitat i/o el tractament correcte de les dades.

11.  Instal·lació  de  programes.  Cap  persona  pot  instal·lar  en  els  sistemes  informàtics  o  descarregar d’Internet,  reproduir,  utilitzar  o  distribuir  programes,  inclosos  els  estandarditzats  i  els  de  caràcter gratuït, sense autorització prèvia i expressa.

12.  Sistemes  externs. No  s’autoritza  la  utilització  de  serveis  informàtics  externs,  com  ara  serveis d’emmagatzematge  al  núvol,  que  no  hagin  estat  aprovats  i/o  contractats  pel  despatx.  No  es  poden utilitzar suports de còpia per guardar-hi informació d’accés restringit si no són suports proporcionats pel despatx.

13. Usos dels sistemes d’informació. La seguretat de les dades personals i el compliment general de la normativa  sobre  protecció  de  dades  exigeix  un  ús  correcte  dels  recursos  informàtics.  No  es  poden utilitzar  de  forma  que  interfereixin  les  tasques  i funcions  assignades  ni  el  normal  funcionament d’aquests  recursos  informàtics.  Tots  els  recursos  i  sistemes  que  es  poden  a  disposició  del  personal, inclòs  el  correu  electrònic  corporatiu  i  l’accés  a Internet,  són  eines  de  treball  que  han  de  servir exclusivament per a desenvolupar les tasques i funcions assignades a cadascú. No es poden destinar a altres finalitats.  

14.  Verificacions. Es verifica regularment la correcta utilització d’aquests recursos i sistemes. Aquesta comprovació  es  fa    de  forma  respectuosa  amb  els  drets  dels  treballadors.  Es  poden  fer  controls automatitzats  sobre  l’accés  a  internet  i  l’ús  del  correu  electrònic,  per  tal  de  vetllar  pel  normal funcionament del sistema (volum de trànsit, volum dels missatges enviats, etc.) i per tal de verificar que efectivament s’utilitzen per a les finalitats pròpies.

------------------------------------------------------------------------------------------------------------------------------------------------------------
Recepció de la informació sobre tractament de dades i sobre funcions i obligacions
La persona sotasignant declara haver rebut el document on se m’informa del tractament de les meves dades que es porta a terme al despatx de [nom del col·legiat/da], així com de les funcions i obligacions que em corresponen en relació al tractament de dades personals.
Acuso rebut d’aquest document a [localitat] el dia [data completa].



Nom i signatura
