# CERTIFICATS i CONFIANÇA

**Creeu una màquina virtual per a les pràctiques. Utilitzeu snapshots per poder tornar enrere els canvis.**

**Feu les passes següents, amb Firefox i Chrome simultàniament (haureu de buscar com es fa amb el Chrome, seguint la pauta del Firefox).**

**Aneu a https://moodle.iescarlesvallbona.cat i comproveu que és de confiança.**

Per comprovar que es de confiança en el cas de Firefox nomes li donem al candat i t'apareixera que es de confiança

![](assets/cert-1.png)

En google chrome fem clic en el candat que apareix a dalt a la esquerra.

![](assets/cert-2.png)

Feu les passes necessàries (captures de les passes) per recopilar la informació que es demana:
**1. Qui emet el certificat d'aquesta entitat?**

**Firefox:**
Li donem al candat després a la fletxa i t'apareixera la entitat.

![](assets/cert-3.png)

**Google Chrome:**

Li donem al candat despres a la opció "Certificado" i t'apareixera.

![](assets/cert-4.png)

**2. Quina és la data de validesa del certificat?**


**Firefox:**
Li donem al candat després a la fletxa per últim a "More information"

![](assets/cert-6.png)

**Google Chrome:**

Li donem al candat després a la opció "Certificado" i t'apareixera.

![](assets/cert-7.png)


**3. Es pot confiar en ell? Per què?**

Perquè  es una entitat de certificació que opera en benefici public.  

**4. Quina és l'empremta digital SHA-1 del certificat? Per a què serveix?**

Es un hash de criptografia. Per calcular el hash de les criptografies publiques.

Firefox:

![](assets/cert-11.png)

Google Chrome:

![](assets/cert-10.png)

5. Exporta el certificat a un fitxer i mostra'l en format text amb l'editor gedit (o qualsevol altre)

Firefox:

![](assets/cert-12.png)

Google Chrome:

![](assets/cert-8.png)

6. Quina és la jerarquia de certificats?

Firefox:

![](assets/cert-25.png)

Google Chrome:

![](assets/cert-9.png)

**Aneu al navegador i esborreu el certificat d'entitat de Let's Encrypt. A partir d'ara serà una entitat NO confiable.**

Per fer-ho en el cas de Firefox hem d'anar a "Preferences"

![](assets/cert-13.png)

Ens anem a "Privacy & Security" i després "View Certificates"

![](assets/cert-14.png)

I esborrem el certificat.

**Aneu a Opciones->Botó Avanzados->Pestanya Certificados->Ver certificados  i a la pestanya "entitats" importeu el certificat de Let's Encrypt.Torneu a obrir el navegador i comproveu si es torna a confiar.**
En aquest cas si.

### Xifrat del correu electrònic
**Configura el client de correu Thunderbird per a poder enviar i rebre missatges de correu electrònic xifrats i i firmats.**
1- Primer de tot afegim una compte  de correu electronic, en el meu cas he afegit la meva compte de gmail.

![](assets/thun-1.png)

2- Inhabilitem el correu electronic HTML
Anem a "Opcions"-"Visualitza-Cos del missatge-Text net"
3- Inhabilitem la visualització del correu electrònic HTML

Anem a els paràmetres del compte i la configuració "Redacció i adreçament" ho deixem així

![](assets/thun-4.png)

4- Configurar els paràmetres de seguretat del Thunderbird
Anem a "Opcions-Preferències"-"Seguretat-Contrasenyes" i eliminem totes les Contrasenyes després li donem a "OK" i escollim l'opció "utilitza una contrasenya maestra"

![](assets/thun-7.png)

Fet això ens tindria que aparèixer aquesta opció marcada

![](assets/thun-8.png)

5- Configurar la privadesa en el Thunderbird.

Anem a "Opcions-Preferències"-"Privadesa" i ho configurem tal que així


![](assets/thun-9.png)

6- Enviar i rebre correu electrònic xifrat.

6.1 Instal·lem "Enigmail"

Anem a complements i després busquem "Enigmail", fet aixó li donem a "Afegeix al Thunderbird"

![](assets/thun-10.png)

Una vegada fet això tanquem la finestra i ens tindria que aparèixer en "Extensions" el "Enigmail"

![](assets/thun-11.png)

Quan instal·lem el "Enigmail" ja et creara la clau automàticament i el certificat.

![](assets/thun-12.png)

6.2 Configurar enigmail per a utilitzar-ho amb la teva compte de correu electrònic.

Anem a "Opcions"-"Preferències"-"Paràmetres dels comptes" i anem a opció "Seguretat OpenPGP"

I deixem la configuració com aquesta captura.

![](assets/thun-13.png)

**IMPORTANT**

En la opció "Select Key" el numero sempre varia perquè no es la mateixa clau per tot@s.

6.3 - Comprovacions
En aquest cas jo m'he enviat un correu a mi mateix per provar-ho. Quan estàs redactant el missatge a dalt et tenen que aparèixer un candat i un bolígraf Il·luminats

![](assets/thun-14.png)


![](assets/thun-15.png)



**Configura el navegador web (Chrome o Firefox) per poder enviar o rebre correu xifrat i/o firmat des de clients Webmail, utilitzant l’extensió Mailvelope.  Fes servir la guia**

1- Instal·lem la extensió "Mailvelope" en el firefox

2- Ens anem a la extensió i li donem a la opció "Start" ens sortira una finestra. Primer de tot li donem a la opció "Generate Key"


![](assets/thun-16.png)

Llavors en aquesta finestra posem el nostre nom, correu i la contrasenya d'aquest, una vegada posat tot li donem a "Generate"

![](assets/thun-17.png)

Si s'ha fet be ens tindria que aparèixer així.


![](assets/thun-18.png)

4- Comprovació

Anem al nostre Webmail en aquest cas es "Gmail" i redactem un missatge, en el missatge ens tindria que aparèixer un icona de la extensió.

![](assets/thun-20.png)
