## Configurar les directives locals
Primer anem a "**Panel de control**"
![](assets/con-1.png)

**"Sistema y seguriad"**
![](assets/con-2.png)

**"Herramientas administrativas"**
![](assets/con-3.png)

**"Directiva de seguridad local"**

![](assets/con-4.png)

**"Carpetas directivas de contraseñas"**.

![](assets/con-5.png)


#### Explicació de cada directiva

"**Directivas de cuenta**"

![](assets/con-9.png)

"**Almacenar contraseñas con cifrado reversible**"

Aquesta directiva el que fa es et xifra les contrasenyes.

"**Exigir historial de contraseñas**"

Aquesta directiva el que fa es que no puguis utilitzar una contrasenya antiga

"**La contrasenya debe cumplir los requisitos de seguridad**"

Aquesta directiva el que fa es que habilita la política de contrasenyes que tu
configuris.

"**Longitud minima de contraseña**"

Aquesta directiva, el que fa es determina la longitud mínima de la contrasenya

"**Longitud maxima de contraseña**"

Aquesta directiva, el que fa es determina la longitud màxima de la contrasenya

"**Vigencia maxima de contrasenya**"

Aquesta directiva el que fa es que determina quan de temps es pot utilitzar la
contrasenya.

"**Vigencia minima de contrasenya**"

Aquesta directiva el que fa es que determina quan de temps mínim
es pot utilitzar la contrasenya.

"**Directivas bloqueo de cuenta**"

![](assets/con-10.png)

"**Duración del bloqueo de la cuenta**"

Aquesta directiva, determina quant temps es queda bloqueja la conta.

"**Restablecer el bloqueo de cuenta despues de**"

Aquesta directiva determina, quants intents d'inici de sessió pots fer

"**Umbral de bloqueo de cuenta**"

Aquesta directiva el que fa es, determina quans intents d'inici de sessió pots
fer abans de que la compte quedi blocada.

### Prova de les directives

En aquest cas he determinat els caràcters mínim que ha de tenir la contrasenya,
per fer-ho he anat a les propietats de la directiva "Longitud minima de la
contraseña" i he canviat a 7 caracters, despres he creat un usuari i al fer
la contraseña amb 6 caracters no m'ha deixat.

**Configuració**
![](assets/con-12.png)

**Prova**

![](assets/con-16.png)

### Configuració de les directives de contrasenyes

Per configurar les directives de les contrasenyes tens que anar al panell de
control, després a "Sistema y Seguridad", "Herramientas administrativas" i
"Directivas de seguriad local".

**Configuració caracters minim**

En aquest cas son 10.

![](assets/con-17.png)

**Configuració  requisits de complexitat**

Primer els he habilitat

![](assets/con-18.png)

**Configuració:**

10 caràcters mínims, te que tindre caràcters alfanumèrics, te que tindre
majúscules i minúscules.

 **Contrasenyes recordades**

 En aquest cas l'usuari no podrà utilitzar les ultimes 3 contrasenyes

![](assets/con-19.png)

**Vigencia màxima de les contrasenyes**

En aquest cas l'usuari només podrà recordar les contrasenyes durant 1 mes

![](assets/con-20.png)

**Vigencia mínima de les contrasenyes**

En aquest cas l'usuari no podra canviar la contrasenya abans de 3 dies

![](assets/con-21.png)

**"Umbral de bloqueo de cuenta"**

Per dir que cada x intents d'inici de sessió el compte es quedi blocat, hem
d'anar a l'opció "umbral de bloqueo de cuenta", dins de "directiva de bloqueo
de cuenta", una vegada estiguem diem quants inici de sessió te, en aquest cas
son 3.

![](assets/con-22.png)


**"Restablecer el bloqueo de cuenta despues de.. "**

En aquest cas, per tornar a iniciar sessió te que passar 5 minuts

![](assets/con-23.png)

**"Duración del bloqueo de cuenta"**

En aquest cas la compte es quedara blocada 3 minuts

![](assets/con-24.png)

Una vegada hem configurat tot aixo, les directives es quedaren configurades
