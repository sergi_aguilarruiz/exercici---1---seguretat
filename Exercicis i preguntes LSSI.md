# Exercicis i preguntes LLSI


**Quins són els criteris bàsics per determinar si una web ha de complir l'LSSI?***

Criteris bàsics:

Que faci compra i venda, subhastes, contratació, i publicitat



**Quins són els serveis exclosos de l'LSSI?**

Telefonia, radiodifusio televisiva, sonora, teletext, correu electronic no economic.


**Quines són les obligacions dels que ofereixin algun servei per internet?**

Les seves obligacións son: Garantir la constància registral, Oferir informació permanent sobre la empresa els productes i la seves capacitats, coŀlaborar amb les autoritats i retenir les dades de trànsit


**Quines són les dades que ha de mostrar el propietari d'una web subjecta a l'LSSI segons l'obligació d'oferir informació?**

Les dades son:

- Nom de l'empresa
- Adreça
- NIF
- Forma de contacte (correu, telefon...)
- Dades d'inscripció registral
- Codis de conducta


**En intentar demanar informació sobre qui és el propietari de la web, ens han dit que només ho farà si li paguem 2€ perquè protegeix les seves dades
personals segons la LOPD. És correcte? Quina sanció li podria caure?**

No es correcte, la sanció que li podria caure seria la de no informar de forma permanent, fàcil, directa i gratuïta de les dades exigides legalment, la sanció econòmica seria fins a  30.000€

**Volem comprar un producte que només està disponible en una web de Hong Kong. Però a l'hora de pagar no ens diu quines seran les despeses de transport.**

**El podem denunciar per incompliment de l'LSSI?**

Si perque segons la LSSI te la obligació de informar que se li cobrara per el viatge exactament.


**Defineix el “comerç electrònic”.**

Es la producció, venda, distribució, comercialització i lliurament de bens i serveis per mitjans electrònics.

**Indica cinc exemples d'activitats que creguis que es poden dur a terme mitjançant el comerç electrònic.**

- Oferir un servei en streaming per veure pel·lícules i series (netflix)
- Vendre productes de segona ma
- Oferir un servei de hosting o cloud hosting
- Vendre coches de segona ma o nous.


**Defineix “correu brossa (SPAM)” i comenta breument les sancions associades.**

SPAM:

- Es el correu electrònic que una persona rep quan no ha sol·licitat, aquests correus solen ser amb publicitat
Sancions:

- Enviament massiu de comunicacions comercials per correu electronic sense autorització

- Enviament correus no autoritzats o demanats

- La sanció econòmica per SPAM pot anar fins els 150.000€

**Indica les propietats de la signatura digital.**

Propietats:

- Mes Seguretat
- Menys diners
- No utilitza paper
- Millor experiencia del client

**Explica la diferència entre signatura digital i signatura electrònica.**

La firma digital el seu objectiu no es fer un acte de voluntat

**Defineix el “correu segur”, explica'n breument les propietats.**

El correu segur bàsicament son els correus que s'envien encriptats

Propietats: Correu encriptat, mes seguretat, reducció del phising, reducció del SPAM

**Indica tres exemples en els quals creguis que és necessari l'ús del correu electrònic segur. Explica'n breument el perquè.**

- Les empreses, perquè si s'envien dades personals o informació sensibles doncs per a poder prevenir el robatori d'aquestes
- Anti-Phising: Al tenir el correu segur disminueixes molt el Phising
- Anti-Spam: Disminuició de correu SPAM

**Quina relació hi ha entre el correu segur, la privadesa de l'usuari i la signatura electrònica?**
T'assegures de que el document que rebis o enviïs sigui segur.

## Exercicis i preguntes LPI

**Definiu els trets característics de cadascuna de les següents llicències, enllaçant un exemple un exemple d'obra que la usi:**

**Llicència propietària.**
La llicència propietària es aquella llicència que es domini d'algú, no es pot alterar ni modificar el codi font. Limiten la distribució i l'ús, un exemple es Windows
**Llicència de codi obert.**
Amb aquesta llicència es pot fer el que vulguis amb el programa. Ubuntu
**Llicència de software lliure.**
Amb aquesta llicencia es pot fer el que vulguis amb el programa però si l'alteres i la comparteixes te que ser amb la mateixa llicencia Unix
**Llicència Creative Commons:**

**Només atribució (CC BY).**
L'autor ha de ser reconegut
**Atribució i compartir igual (CC BY-SA).**
L'autor ha de ser reconegut, i s'ha de compartir amb la mateixa llicencia
**Atribució i No Derivades (CC BY-ND).**
L'autor ha de ser reconegut,s'ha de compartir amb la mateixa llicencia, no hi pot haver-hi obres derivades
**Atribució i No Comercial (CC BY-NC).**
L'autor ha de ser reconegut i no es pot comercialitzar
**Atribució, No Comercial i Compartir Igual (CC BY-NC-SA).**
L'autor ha de ser reconegut,s'ha de compartir amb la mateixa llicencia, i no es pot comercialitzar
**Atribució, No Comercial i No Derivades (CC BY-NC-ND).**
L'autor ha de ser reconegut,no es pot comercialitzar i no hi pot haver-hi obres derivades

**Domini Públic**
Quan l'autor d'una obra mort, i passen 70 anys l'obra passa a ser domini public, això vol dir que tothom pot utilitzar aquesta obra.  
