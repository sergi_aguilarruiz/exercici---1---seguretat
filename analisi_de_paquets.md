# Anàlisi de paquets

## Filtres

**Ex 1**

Sintaxi: tcp.port==21

Paquets:

Per contar-los apliquem el filtre, després anem a "File" i per últim a "Export Specified Packets"

![](assets/wire-2.png)


![](assets/wire-1.png)

**Ex 2**

Sintaxi: ip.addr == 192.168.1.147

Paquets:

![](assets/wire-3.png)

![](assets/wire-4.png)

**Ex 3**

Sintaxi: dns && ip.addr == 192.168.1.147

Paquets:

![](assets/wire-5.png)

![](assets/wire-6.png)

5 Dominis consultats:

1- g.api.mega.co.nz

2- localhost.megasyncloopback.mega.nz

3- ftp.suse.com

4- detectportal.firefox.com

5- start.ubuntu.com

**Ex 4**

Sintaxi: !db-lsp-disc && eth.addr == ff:ff:ff:ff:ff:ff

Paquets:

![](assets/wire-8.png)

![](assets/wire-7.png)

Protocols:

ARP

BROWSER

DHCP

NBNS

## Protocols

**Ex 5**

MAC: 14:da:e9:62:5f:7c

Gateway: d4:76:ea:0f:fd:58

**Ex 6**

User: anonymous

PASS: contra

![](assets/wire-10.png)

Ultim fitxer descarregat:

README.txt

![](assets/wire-12.png)

**Ex 7**

Sintaxi: tcp.port==443

Intercanvi de certificats.

![](assets/wire-15.png)

**Ex 8**

Títol de la pel·lícula:

Episode IV



Fragment de la pelicula:

![](assets/wire-16.png)

**Ex 9**

Nom:

towel.blinkenlights.nl

**Ex 10**

Aquesta es la IP de la maquina virtual que esta en mode promiscu. 

![](assets/wire-17.png)
