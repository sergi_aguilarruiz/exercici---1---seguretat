# Gestió de particions MBR en Windows

## Gestió gràfica Windows

**3 particions en el disc nou de 1000MB amb format NTFS**

Per fer una partició primer ens tenim que anar a "administrador de discos"

![](assets/part-1.png)

Ara identifiquem el disc, en aquest cas el de nosaltres es el "Disco 1", llavors ara fem clic dret en el disc i escollim l'opció "Nuevo volumen simple"

![](assets/part-10.png)


Llavors s'obrira un asisstent quan posem el tamany de la partició aquest ha de ser de 1024MB

![](assets/part-11.png)


A l'hora de posar el format aquest ha de ser NTFS i el format rapid

![](assets/part-12.png)

Ara hem de fer 2 mes amb aquest format i el mateix tamany una vegada fet tot això hauria de quedar així

![](assets/part-13.png)

Ara hem de afegir una 4º partició però sense format ràpid, per fer-ho en la finestra on dones el format només tens que desmarcar aquesta opció

![](assets/part-14.png)

**Diferencia de temps:** En aquest cas al no fer un format ràpid doncs tarda una mica mes. En concret aproximadament 1 minut.

**La partició 4 és igual a les anteriors? Quina diferència hi ha?**

No, es una partició lògica.

Es poden continuar fent particions? Crea'n 3 més de 1000 MB.

Si, però aquestes seran lògiques

Resultat final:  

![](assets/part-15.png)

### Gestió des de consola Windows

Per esborrar una partició primer obrir el CMD i escrivim la comanda "diskpart"
~~~
C:\Users\saguilar>diskpart
~~~

Una vegada a dins seleccionem el disc on hem fet les particions en aquest cas es amb la següent comanda:
~~~

DISKPART> list disk

  Núm Disco  Estado      Tamaño   Disp     Din  Gpt
  ---------- ----------  -------  -------  ---  ---
* Disco 0    En línea         50 GB      0 B
  Disco 1    En línea         20 GB    12 GB

  DISKPART> select disk 1

  El disco 1 es ahora el disco seleccionado.
  ~~~

~~~
DISKPART> list partition

  Núm Partición  Tipo              Tamaño   Desplazamiento
  -------------  ----------------  -------  ---------------
  Partición 1    Principal         1024 MB  1024 KB
  Partición 2    Principal         1024 MB  1025 MB
  Partición 3    Principal         1024 MB  2049 MB
  Partición 0    Extendido           16 GB  3073 MB
  Partición 4    Lógico            1024 MB  3074 MB
  Partición 5    Lógico            1024 MB  4099 MB
  Partición 6    Lógico            1024 MB  5124 MB
* Partición 7    Lógico            1024 MB  6149 MB

DISKPART> delete partition

DiskPart eliminó correctamente la partición seleccionada.

~~~

**Comprovació**

![](assets/part-16.png)

## Gestió de particions MBR en Linux

**Partició primaria de 1000MB**
Per fer la partició primer obrim el menu desplegable i seleccionem el disc, després li donem clic dret sobre el disc i escollim l'opció "nueva"

![](assets/part-20.png)

Ens demana crear abans una taula de particións per tant anem a "Dispositivo", "Crear tabla de particiones" i el tipus sera "msdos"

![](assets/part-21.png)

Ara creem la primera partició, en aquesta finestra posem 1024MB,el sistema d'arxius "NTFS" i el nom "p1"

![](assets/part-22.png)

**Quins sistemes d'arxius hi ha disponibles? Quin nom de partició te? Com s'hauria de dir?**

![](assets/part-23.png)

El nom es p1, s'hauria de dir **/dev/sdx1**

**Ara crea una nova partició estesa amb la resta de l'espai lliure. Quin nom de partició te? Quin hauria de tenir?**

El nom que hauria  de tenir es **/dev/sdx4**


**Crea una partició lògica "p2" de 1000 MB amb format EXT4. Quin nom de partició te? Quin és el nom que hauria de tenir?**

![](assets/part-27.png)

EL nom es p2, hauria de tenir **/dev/sdx5**

**Crea una partició lògica "p3" de 1000 MB amb format FAT32. Quin nom de partició te?**

![](assets/part-28.png)

El nom de partició es p3

**Intenta redimensionar la partició "p2" per que ocupi 2000 MB. Et deixa? Què hauràs de fer per que et deixi?**

No, eliminar la partició i fer un altre.

![](assets/part-29.png)

### Gestió en entorn consola Linux

**Obre una consola i executa "sudo fdisk -l".
Quina informació obtens? Descriu-la amb les teves paraules.**

Al executar aquesta comanda et surt els discs i la informació de les seves particions


**Fes un llistat de particions de /dev/sdb.**

~~~
Orden (m para obtener ayuda): p
Disco /dev/sdb: 8 GiB, 8589934592 bytes, 16777216 sectores
Unidades: sectores de 1 * 512 = 512 bytes
Tamaño de sector (lógico/físico): 512 bytes / 512 bytes
Tamaño de E/S (mínimo/óptimo): 512 bytes / 512 bytes
Tipo de etiqueta de disco: dos
Identificador del disco: 0xa85e95a1

Dispositivo Inicio Comienzo    Final Sectores Tamaño Id Tipo
/dev/sdb1              2048  2099199  2097152     1G  7 HPFS/NTFS/exFAT
/dev/sdb2           2099200 16777215 14678016     7G  5 Extendida
/dev/sdb5           4200448  6297599  2097152     1G  b W95 FAT32
/dev/sdb6           2101248  4198399  2097152     1G 83 Linux
~~~
**Esborra la partició "p1" i executa els canvis.**

~~~
Orden (m para obtener ayuda): d /dev/sdb1
Número de partición (1,2,5,6, valor predeterminado 6): 1

Se ha borrado la partición 1.
~~~

**Quan vas a gparted es veuen les noves particions? Si no, actualitza la pantalla de gparted refrescant els dispositius.**

![](assets/part-30.png)

**Quin sistema d'arxius té la partició p4? Formata la partició per tenir sistema d'arxius ext3.**

El sistema es ext4

![](assets/part-31.png)

**Esborra la partició estesa. Et deixa? Què hauries de fer per poder-ho fer? Actualitza els canvis i tindràs el disc buit de nou.**

No, lo que hauria de fer es esborrar totes les particions lògiques i després esborrar la extensa.

![](assets/part-32.png)

**Crea 4 particions primàries de 1000 MB**

![](assets/part-36.png)

**Crea una partició primària més. Et deixa? Què hauries de fer per poder continuar creant particions? Fes-ho**.

No, hauria de eliminar una partició primaria per crear una extensa

![](assets/part-37.png)
