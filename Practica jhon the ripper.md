## Documentació Exercicis d'obtenció de contrasenyes
Primer muntem el disc que hem descarregat en una maquina virtual,
 després executem "Kali live" en aquella maquina virtual.

_**Maquina virtual amb kali i amb el disc dur que hem descarregat.**_

![](assets/jhon1.png)

Quan iniciem la maquina virtual amb **kali**, hem de fer la comanda.

`"ldisk-l"`

Serveix per veure els dispositius disponibles en la maquina virtual,
si el disc l'hemmuntat be en el camp **"type"** ens tindria que sortir,
 **"Linux LVM".**

![](assets/jhon2.png)

Una vegada fet això, hem de muntar el disc en el sistema arrel de
la maquina llavors executem la **comanda**

`vgscan`

aquesta comanda serveix
per llegir tots els volums físics que hi han al sistema.

![](assets/jon3.png)

**Ara executem la comanda**

 `vgchange -ay metasploitable `

Aquesta comanda serveix per activar els volums lògics del grup "metasploitable"

![](assets/jhon4.png)

**Ara executem la comanda**

`lvs`

Aquesta serveix per a buscar els volums lògics del root.

![](assets/jhon6.png)


**A continuació executem la comanda**

`mkdir disco`

Aquesta comanda serveix per crear una carpeta en el directori actual,
en aquest cas el directori es **/root** i la carpeta es diu **"disco"**.

![](assets/jhon5.png)

Ara muntarem en la carpeta que hem fet, el disc que hem descarregat per fer-ho
**executem la següent comanda**

`mount /dev/metasploitable/root disco`

![](assets/jhon9.png)


Una vegada hem muntat el disc copiem els fitxers de
les contrasenyes en un directori diferent, en aquest cas el directori es:

![](assets/jhon10.png)

**Executem la següent comanda per copiar el fitxers**

`cp shadow /copia-fitxers`
`cp passwd /copia-fitxers`


![](assets/jhon11.png)
![](assets/jhon12.png)

Una vegada fet això hem de convertir el fitxer shadow en un fitxer de
contrasenyes
normal per fer-ho ens situem en el directori on està el disc i executem la següent comanda

`unshadow passwd shadow > r00t4john`

![](assets/john13.png)

Ara executem el john en el fitxer que hem obtingut, per fer-ho fem
la **següent comanda**

`# john --single r00t4john`

![](assets/jhon14.png)

Ara executem el jhon que només utilitzi caracters alfabètics per esbrinar
les contrasenyes, per fer-ho **executem la següent comanda**

`john --incremental:alpha r00t4john`

![](assets/jhon15.png)


**Ara executem la següent comanda**

` john --wordlist=/usr/share/wordlists/500-worst-passwords.txt--rules r00t4john`

Aquesta comanda el que fa es executa el john amb un diccionari i tambe
l'executem per a que crei variacions de les contrasenyes
Una vegada fet això ens tindria que esbrinar aquestes contrasenyes

![](assets/jhon20.png)

Ara executem el john amb la llista "rockyou.txt" que es una llista molt
mes complexa, per fer-ho **executem la següent comanda**

`john --wordlist=/usr/share/wordlists/rockyou.txt --rules r00t4john`

![](assets/jhon20.png)

Ara executem la comanda

`john --show r00t4john`

Aquesta comanda serveix per veure tots els usuaris i contrasenyes que hem obtingut

![](assets/jhon20.png)

### Taula

**Root  | Sys  |  klog |   msfadmin | postgres  | user  | service  **
--|---|---|---|---|---|--
usuari  | batman  |  klog |  msfadmin | postgres  |  user |  service |
