# OpenVPN amb PfSense

### Interficies del firewall.

En aquest cas hi ha una interna i una interna.

![](assets/vpn-1.png)

Després hem de tenir un client connectat a la xarxa LAN del firewall.

### Configuració de servidor VPN.

1- Ens connectem al pfesense en aquest cas amb aquesta URL, "https://10.10.0.1", despres anem a "VPN-OpenVPN-Wizards, una vegada aquí li donem a "Next"

![](assets/vpn-2.png)

Llavors en aquesta finestra posem les dades del nostre certificat, en aquest cas he posat aquestes dades.

![](assets/vpn-3.png)

Aquí creem el certificat en aquest cas ho he deixat tal que així.

 ![](assets/vpn-4.png)

En aquesta finestra configurem els parametres, en aquest cas nomes utilitzarem el protocol UDP i el port 1194.

 ![](assets/vpn-5.png)

  ![](assets/vpn-6.png)

  ![](assets/vpn-8.png)

Per últim li donem a "Next"
  ![](assets/vpn-9.png)

En aquesta finestra marquem les dos opcions, i despres li donem a "Finish"

![](assets/vpn-11.png)

Quan acabem ens tindria que sortir aixó

![](assets/vpn-12.png)

### Crear usuari i certificat .

1- Anem a "System- User Manager" i li donem a "Add"

![](assets/vpn-13.png)

2- Aquí posem els paràmetres del usuari en aquest cas ho deixem així

![](assets/vpn-14.png)

![](assets/vpn-15.png)

### Exportar el fitxer  de configuració del client

1- Ens descarreguem aquest paquet, per anar aqui hem d'anar a "System-Package_Manager-Package-Installer" i busquem "Client".  

![](assets/vpn-16.png)

![](assets/vpn-17.png)

Ara anem a "VPN-OpenVPN-Serves-Client-Export" anem fins abaix i ens descarreguem en el client que utilitzara la VPN l'opció "Most Clients".

![](assets/vpn-18.png)


### Connectar-se a la VPN desde el client amb el terminal.

Quan estigui descarregat instal·lem el OpenVPN amb aquesta comanda.

~~~
sudo apt install openpvn
~~~

Ara anem al directori on està l'arxiu que ens baixat en aquest cas es en "Baixades" i executem aquesta comanda.

![](assets/vpn-19.png)

Ens demanara un usuari i la contrasenya, en aquest cas posem el que hem creat abans, el nom es "usuari" i la contrasenya es "futbol123".

~~~
sergi_aguilarruiz@usuari-vallbona:~/Baixades$ sudo openvpn --config pfSense-udp-1194-usuari-config.ovpn
Thu Mar 12 09:34:59 2020 OpenVPN 2.4.4 x86_64-pc-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on May 14 2019
Thu Mar 12 09:34:59 2020 library versions: OpenSSL 1.1.1  11 Sep 2018, LZO 2.08
Enter Auth Username: usuari
Enter Auth Password: *********
Thu Mar 12 09:35:05 2020 TCP/UDP: Preserving recently used remote address: [AF_INET]172.16.100.156:1194
Thu Mar 12 09:35:05 2020 TCP/UDP: Socket bind failed on local address [AF_INET][undef]:1194: Address already in use (errno=98)
Thu Mar 12 09:35:05 2020 Exiting due to fatal error
sergi_aguilarruiz@usuari-vallbona:~/Baixades$ sudo openvpn --config pfSense-udp-1194-usuari-config.ovpn
Thu Mar 12 09:35:22 2020 OpenVPN 2.4.4 x86_64-pc-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on May 14 2019
Thu Mar 12 09:35:22 2020 library versions: OpenSSL 1.1.1  11 Sep 2018, LZO 2.08
Enter Auth Username: usuari
Enter Auth Password: *********
Thu Mar 12 09:35:26 2020 TCP/UDP: Preserving recently used remote address: [AF_INET]172.16.100.156:1194
Thu Mar 12 09:35:26 2020 UDP link local (bound): [AF_INET][undef]:1194
Thu Mar 12 09:35:26 2020 UDP link remote: [AF_INET]172.16.100.156:1194
Thu Mar 12 09:35:26 2020 WARNING: this configuration may cache passwords in memory -- use the auth-nocache option to prevent this
Thu Mar 12 09:35:26 2020 [Saguilar] Peer Connection Initiated with [AF_INET]172.16.100.156:1194
Thu Mar 12 09:35:27 2020 AUTH: Received control message: AUTH_FAILED
Thu Mar 12 09:35:27 2020 SIGTERM[soft,auth-failure] received, process exiting
sergi_aguilarruiz@usuari-vallbona:~/Baixades$ sudo openvpn --config pfSense-udp-1194-usuari-config.ovpn
Thu Mar 12 09:36:05 2020 OpenVPN 2.4.4 x86_64-pc-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on May 14 2019
Thu Mar 12 09:36:05 2020 library versions: OpenSSL 1.1.1  11 Sep 2018, LZO 2.08
Enter Auth Username: usuari
Enter Auth Password: *********
Thu Mar 12 09:36:09 2020 TCP/UDP: Preserving recently used remote address: [AF_INET]172.16.100.156:1194
Thu Mar 12 09:36:09 2020 UDP link local (bound): [AF_INET][undef]:1194
Thu Mar 12 09:36:09 2020 UDP link remote: [AF_INET]172.16.100.156:1194
Thu Mar 12 09:36:09 2020 WARNING: this configuration may cache passwords in memory -- use the auth-nocache option to prevent this
Thu Mar 12 09:36:09 2020 [Saguilar] Peer Connection Initiated with [AF_INET]172.16.100.156:1194
Thu Mar 12 09:36:10 2020 TUN/TAP device tun0 opened
Thu Mar 12 09:36:10 2020 do_ifconfig, tt->did_ifconfig_ipv6_setup=0
Thu Mar 12 09:36:10 2020 /sbin/ip link set dev tun0 up mtu 1500
Thu Mar 12 09:36:10 2020 /sbin/ip addr add dev tun0 10.0.8.3/24 broadcast 10.0.8.255
Thu Mar 12 09:36:10 2020 Initialization Sequence Completed
~~~

**Comprovació**

Entrar al pfsense des de la maquina física o fer ping a la ip del pfsense.

![](assets/vpn-20.png)

Fer un ip a i comprovar que ens ha creat una interfície tunel amb la VPN.

~~~
4: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 100
    link/none
    inet 10.0.8.3/24 brd 10.0.8.255 scope global tun0
       valid_lft forever preferred_lft forever
    inet6 fe80::baeb:fa3e:d51d:f152/64 scope link stable-privacy
       valid_lft forever preferred_lft forever
~~~

### Connectar-se a la VPN des de el client amb el Network Manager.

Instal·lar el paquet de Network Manager.

~~~
sergi_aguilarruiz@usuari-vallbona:~/Baixades$ sudo apt-get install network-manager-openvpn-gnome
~~~

Anem als parametres de xarxa, fem clic a la fletxa on posa "VPN" i li donem exporta des d'un fitxer.

![](assets/vpn-21.png)

Escollim el fitxer.

![](assets/vpn-22.png)

En aquesta finestra deixem tot predeterminadament meys el usuari que en aquest cas es "usuari" i la contrasenya "futbol123"

![](assets/vpn-23.png)


L'activem i comprovem que podem accedir al pfsense.

**Comprovació.**

![](assets/vpn-24.png)

![](assets/vpn-25.png)

Ip a desde el terminal.

~~~
6: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 100
    link/none
    inet 10.0.8.3/24 brd 10.0.8.255 scope global noprefixroute tun0
       valid_lft forever preferred_lft forever
    inet6 fe80::8084:7cc4:3cb5:4080/64 scope link stable-privacy
       valid_lft forever preferred_lft forever
sergi_aguilarruiz@usuari-vallbona:~$
~~~

### VPN des de Windows 10.

Windows suporta aquestes VPN.

![](assets/vpn-40.png)

Fem una maquina virtual amb Windows 10 i la posem amb adaptador pont.

Fem un usuari nou per el windows 10, quan ho estiguem creant hem de fer un "user certificate."

![](assets/vpn-26.png)

Posem el nom que vulguem i la autoritat certificadora la deixem com està.

![](assets/vpn-27.png)

Ara anem a "Client-OpenVPN-Client Export" anem fins abaix i ens fixem que hi han 2 perfils un per l'usuari del ubuntu i un per el windows 10, doncs ens baixem el del windows10. En aquest cas es "Most Clients"

![](assets/vpn-28.png)

Quan el baixem ens el tenim que pasar a la maquina virtual de windows 10, per fe-ho ho pasem per el gmail.

Ara anem a la maquina i ens descarreguem el open-vpn.

![](assets/vpn-29.png)

Una vegada instal·lat li donem a la fletxa i després clic dret a l'ordinador amb un candau, i a "import file" en aquest cas seleccionem el fitxer de la VPN que ens passat per Gmail i ens hem baixat.

![](assets/vpn-31.png)


![](assets/vpn-32.png)

Ara li donem clic dret al ordinador amb el candau i després a "conectar"

![](assets/vpn-33.png)

Ara ens pedira l'usuari i la contrasenya en aquest cas es "sergi" i "futbol123"

![](assets/vpn-35.png)

Aquí li donem a "Permitir"

![](assets/vpn-256.png)

**Comprovació**

![](assets/vpn-36.png)

![](assets/vpn-37.png)

![](assets/vpn-38.png)
