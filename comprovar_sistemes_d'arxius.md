# COMPROVAR SISTEMES D'ARXIUS

### Comprovar sistemes d'arxius en Windows

Per a comprovar això hem d'anar a **"Administrador de discos"**

![](assets/arx-1.png)

Clic dret sobre el disc sata de 8GB que hem afegit, després a la opció "propiedades" i per últim anem a la finestra "Herramientas" i després a l'opció "Comprobar"

![](assets/arx-2.png)

Li donem clic i en el nostre cas ens diu que no ha trobat cap error en el disc.

![](assets/arx-3.png)

**Què passa si comprovem la unitat C: ?**

Ens surt el mateix missatge

![](assets/arx-4.png)

### Per comprovar el sistema d'arxius des de consola

Per comprovar això obrim el cmd com administrador i executem la següent comanda.

~~~
C:\Windows\system32>chkdsk c:
El tipo del sistema de archivos es NTFS.

ADVERTENCIA: parámetro /F no especificado.
Ejecutando CHKDSK en modo de solo lectura.

Etapa 1: Examen de la estructura básica del sistema de archivos...
  96512 registros de archivos procesados.                                                        04 .
Comprobación de archivos completada.
  2378 registros de archivos grandes procesados.                                          0:00:08 ..
  0 registros de archivos no válidos procesados.                                       :08 ...

Etapa 2: Examen de la vinculación de nombres de archivos...
  173 registros de análisis procesados.                                                 0:00:06 ... ..
  147050 entradas de índice procesadas.                                                       0:00:03 ...
Comprobación de índices completada.
  0 archivos no indizados examinados.                                                  :03
  0 archivos no indizados recuperados en objetos perdidos.                             :03 .
  173 registros de análisis procesados.                                                 0:00:03 ..

Etapa 3: Examen de los descriptores de seguridad...
Comprobación de descriptores de seguridad completada.
  25270 archivos de datos procesados.                                                  0:00 ...
CHKDSK está comprobando el diario USN...
  12642320 bytes de USN procesados.                                                           :00
Se ha completado la comprobación del diario USN.

Se examinó el sistema de archivos sin encontrar problemas.
No se requieren más acciones.

  51833855 KB de espacio total en disco.
  13290408 KB en 68341 archivos.
     60248 KB en 25271 índices.
         0 KB en sectores defectuosos.
    177891 KB en uso por el sistema.
El archivo de registro ha ocupado      65536 kilobytes.
  38305308 KB disponibles en disco.

      4096 bytes en cada unidad de asignación.
  12958463 unidades de asignación en disco en total.
   9576327 unidades de asignación disponibles en disco.
   ~~~

Per examinar la segona unitat posem la següent comanda:

~~~
C:\Windows\system32>chkdsk e:
El tipo del sistema de archivos es NTFS.
La etiqueta de volumen es Nuevo vol.

ADVERTENCIA: parámetro /F no especificado.
Ejecutando CHKDSK en modo de solo lectura.

Etapa 1: Examen de la estructura básica del sistema de archivos...
  256 registros de archivos procesados.                                                         .
Comprobación de archivos completada.
  0 registros de archivos grandes procesados.                                          :02 ..
  0 registros de archivos no válidos procesados.                                       :02 ...

Etapa 2: Examen de la vinculación de nombres de archivos...
  278 entradas de índice procesadas.                                                       0:01
Comprobación de índices completada.
  0 archivos no indizados examinados.                                                  :01 .
  0 archivos no indizados recuperados en objetos perdidos.                             :01 ..
  0 registros de análisis procesados.                                                  :01 ...
  0 registros de análisis procesados.                                                  :01

Etapa 3: Examen de los descriptores de seguridad...
Comprobación de descriptores de seguridad completada.
  11 archivos de datos procesados.                                                     0:00 .

Se examinó el sistema de archivos sin encontrar problemas.
No se requieren más acciones.

   8385535 KB de espacio total en disco.
     46900 KB en 8 archivos.
        72 KB en 13 índices.
         0 KB en sectores defectuosos.
     15007 KB en uso por el sistema.
El archivo de registro ha ocupado      14080 kilobytes.
   8323556 KB disponibles en disco.

      4096 bytes en cada unidad de asignación.
   2096383 unidades de asignación en disco en total.
   2080889 unidades de asignación disponibles en disco.
~~~

**Executeu "chkdsk c: /F". Quina diferència hi ha? Per què?**

Ens surt aquest missatge

~~~
C:\Windows\system32>chkdsk c: /f
El tipo del sistema de archivos es NTFS.
No se puede bloquear la unidad actual.

CHKDSK no se puede ejecutar porque otro proceso ya está usando el
volumen. ¿Desea que se prepare este volumen para que sea comprobado
la próxima vez que se reinicie el sistema? (S/N) s

Este volumen se comprobará la próxima vez que se reinicie el sistema.
~~~

Ens surt aquest missatge perque "f" es el recovery de Windows

### Comprovar sistema d'arxius des d'una distro live d'Ubuntu a Windows 10

Anem al gparted i escollim el primer disc

![](assets/arx-5.png)

Fem clic dret sobre la partició i li donem a "verificar"

![](assets/arx-6.png)

**Disc 2**

![](assets/arx-7.png)

Ara amb l'eina "fsck" per comprovar les particions primer fem aquesta comanda amb les 2 particions de

~~~
sudo fsck -M /dev/sda1
sudo fsck -M /dev/sda2
~~~

Ara executem aquesta comanda amb el altre disc dur

~~~
sudo fsck -M /dev/sdb1
~~~

## Comprovar sistemes d'arxius en Linux

### Per comprovar el sistema d'arxius a l'entorn gràfic

Per fer això anem al gparted, en el menu desplegable escollim el primer disc

![](assets/arx-8.png)

**Escolliu el primer disc, per veure propietats i piqueu botó dret sobre la partició de swap del disc. Intenteu comprovar l'estat. Es pot?**

No es pot

**Feu inactiva la partició d'intercanvi. Ara podeu comprovar la partició? Quin sentit te això?**

Si, això passa perquè alomillor si tu verifiques aquesta partició i no tens memòria l'intercanvi s'activa i per tant si l'intercanvi està activat no es pot fer una comprovació

**Intenteu comprovar la partició de sistema de l'Ubuntu a "sda". Es pot? Arrenqueu una live a la màquina virtual. Ara es pot? Feu-ho si podeu.**

No es pot, si amb la live si que es pot això passa perquè estàs amb l'Ubuntu de la live no del disc

![](assets/Comprovar_sistemes_d'arxius-136d0f8a.png)

**Sortiu de la live i torneu a arrencar del disc dur. Aneu a una finestra de Nautilus (explorador d'arxius) i obriu el disc "sdb" (només te una partició) per a crear a dintre un fitxer de text qualsevol. Com es diu el disc?**

Es diu "Volumen de 8,6GB"

**Ara al gparted comproveu la partició del disc sdb. Es pot? Per què? Si no es pot, feu el necessari per poder comprovar-la.**

No, perque la partició esta activa, per poder verificar la partició he fet un live CD al virtualbox i he iniciat la maquina virtual amb aquest

![](assets/arx-9.png)

**Per comprovar el sistema d'arxius des de consola**

Esborra el fitxer creat abans a sdb des de consola.

Per fer això executem aquesta comanda

~~~
sudo rm -r hola.txt
~~~
**Executeu a consola "fsck /dev/sda1" per comprovar la primera partició del disc "sda". Quin missatge us surt? Creieu recomanable continuar? Expliqueu quina solució hi haurà per poder comprovar la partició (ja hauríeu de saber-ho).**

El missatge es aquest

~~~
/deev/sda1 esta montado
~~~

Doncs primer tindriem que desmuntar la partició amb aquesta comanda

~~~
umount /dev/sda1
~~~

i després fer la verificació

**Executeu "fsck /dev/sdb1" per comprovar la primera partició del disc "sdb". Quin missatge us surt? Us recupera l'arxiu esborrat?**

missatge

~~~
EL sistema de ficheros está montado. SI se continúa se PROVOCARÁN GRAVES daños al sistema de ficheros
~~~
No ho recupera
